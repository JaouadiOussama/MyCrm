import { Route } from '@angular/router';
import { ProjectDetailsComponent } from './project-details.component';

export const projectDetailsRoute: Route = {
  path: 'projectdetails/:id',
  component: ProjectDetailsComponent
  /* data: {
    authorities: [],
    pageTitle: 'accountlist.title'
  }*/
};
