import { Component, OnInit, OnDestroy } from '@angular/core';
import { AjoutProjetModalService } from 'app/shared/services/projetService/ajoutprojet-modal.service';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Opportunity, COpportunity, Stage, Currency, Employee } from 'app/shared/services/projetService/model';
import { HttpResponse, HttpErrorResponse } from '@angular/common/http';
import { Observable, Subscription } from 'rxjs';
import { ToastrService } from 'ngx-toastr';
import { JhiEventManager, JhiParseLinks, JhiEventWithContent } from 'ng-jhipster';
import { ProjectService } from 'app/shared/services/projetService/project.service';
import { TranslateService } from '@ngx-translate/core';

import { DatePipe } from '@angular/common';
import { ContactOpportunity } from 'app/shared/services/opportunityService/module';
import { CurrencyService } from 'app/shared/services/opportunityService/currencie.service';
import { EmployeeProjetService } from 'app/shared/services/projetService/employeeprojet.service';
import { Company } from 'app/shared/services/model';
import { CompanyService } from 'app/shared/services/projetService/company.service';

@Component({
  selector: 'jhi-ajout-projet-modal',
  templateUrl: './projet-ajouter.component.html',
  styleUrls: ['./projet-ajouter.component.scss']
})
export class AjoutProjectModalComponent implements OnInit, OnDestroy {
  showModal = false;
  itemsEtat = [
    { id: 1152, etatNom: 'Termine' },
    { id: 1151, etatNom: 'En cours' },
    { id: 1153, etatNom: 'Annulé' },
    {
      id: 1154,
      etatNom: 'Nouveau'
    }
  ];
  itemsPriorite: string[] = ['Haute', 'Moyenne', 'Basse'];

  isSaving = false;
  enregister = false;
  create = false;
  ContainerClass = 'container';
  dateNow = new Date();

  employees?: Employee[];

  currencies?: Currency[];
  stages: any;

  types = [{ name: 'Client existant' }, { name: 'Client non existant' }];

  selectedStage: any;

  byFirst!: string;
  byResult!: number;

  probabilte!: string;

  registerForm!: FormGroup;

  httpErrorListener!: Subscription;
  selectedContact!: any;

  opportunity?: Opportunity;

  iSdisabled = true;
  contacts?: ContactOpportunity[];
  employee?: Employee;
  selectedEmployee: any;
  companies?: Company[];
  opportunityName?: String;

  constructor(
    protected opportunityService: ProjectService,
    protected ajoutOpportunityModalService: AjoutProjetModalService,
    protected fb: FormBuilder,
    public activeModal: NgbActiveModal,
    protected toastr: ToastrService,
    protected parseLinks: JhiParseLinks,
    protected eventManager: JhiEventManager,
    public translateService: TranslateService,
    private datePipe: DatePipe,
    protected currencyService: CurrencyService,
    protected employeeService: EmployeeProjetService,
    protected companyService: CompanyService
  ) {
    this.httpErrorListener = this.eventManager.subscribe(
      'gatewayServiceApp.httpError',
      (response: JhiEventWithContent<HttpErrorResponse>) => {
        const httpErrorResponse = response.content;
        const entityNameUniqueUrl = this.translateService.instant('opportunity.message.uniqueOppName');

        switch (httpErrorResponse.status) {
          case 400:
            this.toastr.error(entityNameUniqueUrl);

            break;
        }
      }
    );
  }

  ngOnInit(): void {
    this.loadAll();
    this.registerForm = this.fb.group({
      id: [],
      nom: ['', Validators.required],
      montant: [''],
      compte: ['', Validators.required],
      dateDebut: ['', Validators.required],
      dateEcheance: [],
      employee: [],
      prospect: [],
      priorite: [],
      origineProspect: [],
      etape: [],
      chiffreAffaire: [],
      probabilite: [],
      currency: [],
      etat: []
    });
    this.dateNow ? this.datePipe.transform(this.dateNow, 'yyyy-MM-dd') : this.datePipe.transform(this.dateNow, 'yyyy-MM-dd');
    if (this.opportunity !== undefined) {
      this.updateForm(this.opportunity);
      this.opportunityName = this.opportunity?.oppName;
    }
  }

  loadAll(): void {
    this.employeeService.query().subscribe((res: HttpResponse<Employee[]>) => (this.employees = res.body || []));
    this.currencyService.query().subscribe((res: HttpResponse<Currency[]>) => (this.currencies = res.body || []));
    this.companyService.query().subscribe((res: HttpResponse<Currency[]>) => (this.companies = res.body || []));
  }

  show(): void {
    this.ajoutOpportunityModalService.open();
  }

  // Bootstrap Modal Close event
  hide(): void {
    this.showModal = false;
    this.ContainerClass = 'container';
  }

  // convenience getter for easy access to form fields
  // eslint-disable-next-line @typescript-eslint/tslint/config
  get f() {
    return this.registerForm.controls;
  }

  onSubmit(): void {
    this.isSaving = true;
    const opportunity = this.createFormForm();

    if (!this.create) {
      this.subscribeToSaveResponse(this.opportunityService.update(opportunity));
    } else {
      this.subscribeToSaveResponse(this.opportunityService.create(opportunity));
    }
  }

  // creation projet

  private createFormForm(): Opportunity {
    const oppo = new COpportunity();
    oppo.id = this.registerForm.get(['id'])!.value;
    oppo.oppName = this.registerForm.get(['nom'])!.value;
    oppo.projEndate = this.registerForm.get(['dateEcheance'])!.value;
    oppo.projStartdate = this.registerForm.get(['dateDebut'])!.value;
    oppo.projOpp = this.registerForm.get(['montant'])!.value;

    oppo.projPriority = this.registerForm.get(['priorite'])!.value;
    oppo.deleted = 0;
    oppo.createdAt = this.dateNow;
    oppo.updatedAt = this.dateNow;
    if (this.registerForm.get(['currency'])!.value) {
      oppo.currency = this.createCurrency();
    }
    if (this.registerForm.get(['etat'])!.value) {
      oppo.stage = this.createStage();
    }
    if (this.registerForm.get(['compte'])!.value) {
      oppo.company = this.createCompany();
    }

    return oppo;
  }

  protected subscribeToSaveResponse(result: Observable<HttpResponse<Opportunity>>): void {
    result.subscribe(
      () => this.onSaveSuccess(),
      () => this.onSaveError()
    );
  }

  protected onSaveSuccess(): void {
    this.showModal = false;
    const succes = this.translateService.instant('projet.message.create');
    const succes1 = this.translateService.instant('projet.message.update');

    this.isSaving = false;
    if (this.create) {
      this.toastr.success(succes);
    } else {
      this.toastr.success(succes1);
    }

    // this.previousState();
    this.eventManager.broadcast('modifOpportunity');
    this.registerForm.reset();
    this.registerForm.markAsPristine();
    if (this.enregister) {
      this.activeModal.close();
      this.enregister = false;
      this.ContainerClass = 'container';
    }
  }

  protected onSaveError(): void {
    // this.showModal = false;
    this.isSaving = false;
    this.toastr.error('Erreur d ajout projet ');
  }

  ngOnDestroy(): void {
    if (this.httpErrorListener) {
      this.eventManager.destroy(this.httpErrorListener);
    }
  }

  /**
   * update
   */
  updateForm(opportunity: Opportunity): void {
    this.registerForm.patchValue({
      id: opportunity.id,
      nom: opportunity.oppName,

      dateEcheance: opportunity.projEndate,

      dateDebut: opportunity.projStartdate,
      priorite: opportunity.projPriority,
      currency: opportunity.currency?.id,
      etat: opportunity.stage?.id,
      compte: opportunity.company?.id,
      montant: opportunity.projOpp
    });

    this.selectedStage = opportunity.stage;
  }

  cancel(): void {
    this.activeModal.dismiss();
  }

  onClickMe(): void {
    this.enregister = true;
  }

  private createCurrency(): Currency {
    return {
      id: this.registerForm.get(['currency'])!.value
    };
  }

  private createCompany(): Company {
    return {
      id: this.registerForm.get(['compte'])!.value
    };
  }

  private createStage(): Stage {
    return {
      id: this.registerForm.get(['etat'])!.value,
      stageName: this.registerForm.get(['etat'])!.value
    };
  }

  private createEmployee(): Employee {
    return {
      id: this.selectedEmployee?.id
    };
  }
}
