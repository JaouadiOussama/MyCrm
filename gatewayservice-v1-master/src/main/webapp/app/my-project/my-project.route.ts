import { Routes } from '@angular/router';
import { projectListRoute } from 'app/my-project/project-list/project-list.route';
import { projectDetailsRoute } from 'app/my-project/project-details/project-details.route';

const MY_CONTACT_ROUTES = [projectListRoute, projectDetailsRoute];

export const MyProjectState: Routes = [
  {
    path: '',
    children: MY_CONTACT_ROUTES
  }
];
