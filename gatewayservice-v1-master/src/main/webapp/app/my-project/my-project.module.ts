import { NgModule } from '@angular/core';

import { GatewayServiceSharedModule } from 'app/shared/shared.module';
import { RouterModule } from '@angular/router';
import { ProjectListComponent } from './project-list/project-list.component';
import { MyProjectState } from './my-project.route';
import { NgSelectModule } from '@ng-select/ng-select';
import { AjoutProjectModalComponent } from 'app/my-project/projet-ajouter/projet-ajouter.component';
import { ProjectDeleteComponent } from 'app/my-project/project-delete/project-delete.component';
import { ProjectDetailsComponent } from 'app/my-project/project-details/project-details.component';

@NgModule({
  imports: [GatewayServiceSharedModule, NgSelectModule, RouterModule.forChild(MyProjectState)],
  declarations: [ProjectListComponent, AjoutProjectModalComponent, ProjectDeleteComponent, ProjectDetailsComponent],
  entryComponents: [ProjectDeleteComponent]
})
export class MyProjectModule {}
