import { Component, OnInit, OnDestroy } from '@angular/core';
import { Opportunity } from 'app/shared/services/projetService/model';
import { JhiEventManager } from 'ng-jhipster';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { ProjectService } from 'app/shared/services/projetService/project.service';
import { HttpResponse, HttpHeaders } from '@angular/common/http';
import { Subscription } from 'rxjs';
import { AjoutProjetModalService } from 'app/shared/services/projetService/ajoutprojet-modal.service';
import { ProjectDeleteComponent } from 'app/my-project/project-delete/project-delete.component';
import { AjoutProjectModalComponent } from 'app/my-project/projet-ajouter/projet-ajouter.component';

@Component({
  selector: 'jhi-project-list',
  templateUrl: './project-list.component.html',
  styleUrls: ['./project-list.component.scss']
})
export class ProjectListComponent implements OnInit, OnDestroy {
  projects: Opportunity[] = [];
  enventSubscriber?: Subscription;
  checkedClicked = false;
  allButtonChecked = false;
  NamesAscending = true;
  beansFilter = '';

  /* deleted */
  toBeDeleted: number[] = [];
  toBeDeletedApi: number[] = [];

  /* pagination */
  page = 1;
  pageSize = 10;
  totalItems = 0;

  constructor(
    protected projectService: ProjectService,
    protected eventManager: JhiEventManager,
    protected modalService: NgbModal,
    protected ajoutprojetModalservice: AjoutProjetModalService
  ) {}

  loadAll(): void {
    this.projectService
      .findByDeleted(0, {
        page: 0,
        size: 100
      })
      .subscribe((res: HttpResponse<Opportunity[]>) => this.onSuccess(res.body, res.headers));
  }
  private onSuccess(projects: Opportunity[] | null, headers: HttpHeaders): void {
    this.totalItems = Number(headers.get('X-Total-Count'));
    this.projects = projects || [];
  }

  ngOnInit(): void {
    this.loadAll();
    this.registerChangeproject();
    if (!this.toBeDeletedApi.length) {
      this.toBeDeleted = [];
    }
  }

  ngOnDestroy(): void {
    if (this.enventSubscriber) {
      this.eventManager.destroy(this.enventSubscriber);
    }
  }
  reload(): void {
    this.loadAll();
    // this.companies = this.companies?.filter((currentValue, index) => !this.toBeDeleted.includes(index));
    this.projects = this.projects?.filter((currentValue, index) => !this.toBeDeleted.includes(index));
    this.toBeDeleted = [];
    this.checkedClicked = false;
    this.allButtonChecked = false;
  }

  registerChangeproject(): void {
    this.enventSubscriber = this.eventManager.subscribe('modifOpportunity', () => this.reload());
  }

  trackId(index: number, item: Opportunity): number {
    // eslint-disable-next-line @typescript-eslint/no-unnecessary-type-assertion
    return item.id!;
  }
  remove(event: any, index: any, indexApi: number): void {
    if (event.target.checked) {
      this.toBeDeleted.push(index);
      this.toBeDeletedApi.push(indexApi);
    } else {
      const idx = this.toBeDeleted.indexOf(index);
      this.toBeDeleted.splice(idx, 1);
      const idxApi = this.toBeDeletedApi.indexOf(indexApi);
      this.toBeDeletedApi.splice(idxApi, 1);
    }
  }

  checkAll(event: any): void {
    if (event.target.checked) {
      this.checkedClicked = true;
      this.allButtonChecked = true;
      for (let i = 0; i < this.projects?.length; i++) {
        this.toBeDeleted.push(i);
      }
    } else {
      this.checkedClicked = false;
      this.allButtonChecked = false;
      this.toBeDeleted = [];
    }
  }

  checkOne(): void {
    //  this.checkedClicked = !this.checkedClicked;
    if (!this.toBeDeleted.length && !this.allButtonChecked) {
      this.checkedClicked = false;
    } else {
      this.checkedClicked = true;
    }
  }
  SortName(): void {
    this.projects = this.projects
      .filter(projects => !this.beansFilter || projects.oppName?.toLowerCase().includes(this.beansFilter.toLowerCase()))
      .sort((a, b) =>
        (a.oppName ? a.oppName : '') < (b.oppName ? b.oppName : '') ? (this.NamesAscending ? -1 : 1) : this.NamesAscending ? 1 : -1
      );
  }

  show(): void {
    const modalRef = this.modalService.open(AjoutProjectModalComponent, { size: 'md', backdrop: 'static' });
    modalRef.componentInstance.create = true;
  }
  delete(): void {
    const modalRef = this.modalService.open(ProjectDeleteComponent, { size: 'md', backdrop: 'static' });
    modalRef.componentInstance.toBeDeletedApi = this.toBeDeletedApi;
    this.toBeDeletedApi = [];
  }
}
