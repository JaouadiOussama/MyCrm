import { Route } from '@angular/router';
import { ProjectListComponent } from 'app/my-project/project-list/project-list.component';

export const projectListRoute: Route = {
  path: 'projectlist',
  component: ProjectListComponent
  /* data: {
    authorities: [],
    pageTitle: 'projectlist.title'
  }*/
};
