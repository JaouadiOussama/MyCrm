import { Component } from '@angular/core';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { COpportunity, Opportunity } from 'app/shared/services/projetService/model';

import { HttpResponse } from '@angular/common/http';
import { ToastrService } from 'ngx-toastr';
import { JhiEventManager } from 'ng-jhipster';
import { ProjectService } from 'app/shared/services/projetService/project.service';

@Component({
  selector: 'jhi-project-delete',
  templateUrl: './project-delete.component.html'
})
export class ProjectDeleteComponent {
  project!: Opportunity;
  toBeDeletedApi: number[] = [];

  // toBeDeleted: number[] = [];
  deleted = false;

  constructor(
    protected projectService: ProjectService,
    public activeModal: NgbActiveModal,
    protected toastr: ToastrService,
    protected eventManager: JhiEventManager
  ) {}

  cancel(): void {
    this.activeModal.dismiss();
    //  this.toBeDeleted=[];
  }
  confirmDelete(): void {
    for (let i = 0; i < this.toBeDeletedApi.length; i++) {
      this.projectService.find(this.toBeDeletedApi[i]).subscribe((res: HttpResponse<Opportunity>) => {
        this.project = res.body || new COpportunity();
        this.project.deleted = 1;
        this.projectService.update(this.project).subscribe(() => {
          this.eventManager.broadcast('modifOpportunity');
        });
      });
    }

    if (this.toBeDeletedApi.length === 1) {
      this.toastr.success('Projet supprimé avec succès');
    } else {
      this.toastr.success(this.toBeDeletedApi.length + ' Projets supprimés avec succès');
    }
    this.deleted = true;
    this.activeModal.close(this.deleted);
  }
}
