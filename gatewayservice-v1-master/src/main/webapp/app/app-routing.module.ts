import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { errorRoute } from './layouts/error/error.route';
import { navbarRoute } from './layouts/navbar/navbar.route';
import { DEBUG_INFO_ENABLED } from 'app/app.constants';
import { Authority } from 'app/shared/constants/authority.constants';

import { UserRouteAccessService } from 'app/core/auth/user-route-access-service';

const LAYOUT_ROUTES = [navbarRoute, ...errorRoute];

@NgModule({
  imports: [
    RouterModule.forRoot(
      [
        {
          path: 'admin',
          data: {
            authorities: [Authority.ADMIN]
          },
          canActivate: [UserRouteAccessService],
          loadChildren: () => import('./admin/admin-routing.module').then(m => m.AdminRoutingModule)
        },

        {
          path: 'myaccount',
          loadChildren: () => import('./my-account/my-account.module').then(m => m.MyAccountModule)
        },
        {
          path: 'mycontact',
          loadChildren: () => import('./my-contact/my-contact.module').then(m => m.MyContactModule)
        },
        {
          path: 'myproject',
          loadChildren: () => import('./my-project/my-project.module').then(m => m.MyProjectModule)
        },
        /*  {
       path: 'myemployee',
         loadChildren: () => import('./my-employee/my-employee.module').then(m => m.MyEmployeeModule)
       },*/

        {
          path: 'account',
          loadChildren: () => import('./account/account.module').then(m => m.AccountModule)
        },
        {
          path: 'myopportunity',
          loadChildren: () => import('./my-sale/my-opportunites/my-opportunities.module').then(m => m.MyOpportunitiesModule)
        },
        {
          path: 'myfinance',
          loadChildren: () => import('./my-finance/my-finance.module').then(m => m.MyFinanceModule)
        },

        /* {
          path: 'myopportunity',
          loadChildren: () => import('./my-sale/my-opportunites/my-opportunities.module').then(m => m.MyOpportunitiesModule)
        },*/

        {
          path: 'mycontact',
          loadChildren: () => import('./my-contact/my-contact.module').then(m => m.MyContactModule)
        },

        ...LAYOUT_ROUTES
      ],
      { enableTracing: DEBUG_INFO_ENABLED }
    )
  ],
  exports: [RouterModule]
})
export class GatewayServiceAppRoutingModule {}
