import { OnInit, Component } from '@angular/core';
import { ContractService } from 'app/shared/services/contractService/contract.service';
import { JhiEventManager } from 'ng-jhipster';
import { Contract } from 'app/shared/services/contractService/module';

import { HttpHeaders, HttpResponse } from '@angular/common/http';

@Component({
  selector: 'jhi-finance-list',
  templateUrl: './finance-list.component.html'
})
export class FinanceListComponent implements OnInit {
  /* dataTable */

  contracts!: Contract[];

  /* pagination */
  page = 1;
  pageSize = 10;
  totalItems = 0;

  constructor(protected contractService: ContractService, protected eventManager: JhiEventManager) {}

  ngOnInit(): void {
    this.loadAll();
  }
  loadAll(): void {
    this.contractService.query().subscribe((res: HttpResponse<Contract[]>) => {
      this.onSuccess(res.body, res.headers);
      //  this.filterAndSortBeans();
    });
  }

  private onSuccess(beans: Contract[] | null, headers: HttpHeaders): void {
    this.totalItems = Number(headers.get('X-Total-Count'));
    this.contracts = beans || [];
  }
}
