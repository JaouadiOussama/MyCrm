import { Route } from '@angular/router';
import { FinanceListComponent } from './finance-list.component';

export const financeListRoute: Route = {
  path: 'financelist',
  component: FinanceListComponent
  /* data: {
    authorities: [],
    pageTitle: 'accountlist.title'
  }*/
};
