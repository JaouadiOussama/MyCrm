import { NgModule } from '@angular/core';

import { GatewayServiceSharedModule } from 'app/shared/shared.module';
import { RouterModule } from '@angular/router';

import { FinanceListComponent } from './finance-list/finance-list.component';
import { MyFinanceState } from './my-finance.route';

@NgModule({
  imports: [GatewayServiceSharedModule, RouterModule.forChild(MyFinanceState)],
  declarations: [FinanceListComponent]
})
export class MyFinanceModule {}
