import { Routes } from '@angular/router';

import { financeListRoute } from './finance-list/finance-liste.route';

const MY_FINANCE_ROUTES = [financeListRoute];

export const MyFinanceState: Routes = [
  {
    path: '',
    children: MY_FINANCE_ROUTES
  }
];
