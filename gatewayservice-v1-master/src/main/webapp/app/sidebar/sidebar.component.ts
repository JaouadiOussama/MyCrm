import { Component, OnInit } from '@angular/core';
import { AccountService } from 'app/core/auth/account.service';

@Component({
  selector: 'jhi-sidebar',
  templateUrl: './sidebar.component.html',
  styleUrls: ['./sidebar.component.scss']
})
export class SidebarComponent implements OnInit {
  isMobile = false;
  constructor(private accountService: AccountService) {}

  ngOnInit(): void {
    $('.sidebar-dropdown > a').click(function(): void {
      $('.sidebar-submenu').slideUp(200);
      if (
        $(this)
          .parent()
          .hasClass('active')
      ) {
        $('.sidebar-dropdown').removeClass('active');
        $(this)
          .parent()
          .removeClass('active');
      } else {
        $('.sidebar-dropdown').removeClass('active');
        $(this)
          .next('.sidebar-submenu')
          .slideDown(200);
        $(this)
          .parent()
          .addClass('active');
      }
    });

    $('#close-sidebar').click(function(): void {
      $('.page-wrapper').removeClass('toggled');
    });
    $('#show-sidebar').click(function(): void {
      $('.page-wrapper').addClass('toggled');
    });

    window.onresize = () => (this.isMobile = window.innerWidth <= 850);
  }
  isAuthenticated(): boolean {
    return this.accountService.isAuthenticated();
  }
}
