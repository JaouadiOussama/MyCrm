import { Component, OnInit } from '@angular/core';
import { Validators, FormBuilder } from '@angular/forms';
import { Company, City, ActivityArea, CCompany } from 'app/shared/services/model';
import { HttpResponse } from '@angular/common/http';
import { Observable, Subscription } from 'rxjs';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { CompanyService } from 'app/shared/services/company.service';
import { ToastrService } from 'ngx-toastr';
import { ModifiercompteModalService } from 'app/shared/services/modifiercompte-modal.service';
import { ActivatedRoute } from '@angular/router';
import { ActivityService } from 'app/shared/services/activity.service';
import { JhiEventManager } from 'ng-jhipster';

@Component({
  selector: 'jhi-account-modifier-modal',
  templateUrl: './account-modifier.component.html',
  styleUrls: ['./account-modifier.component.scss']
})
export class ModifiercompteModalComponent implements OnInit {
  companies?: Company[];
  EditCompany: any;
  enventSubscriber?: Subscription;
  eventSubscriber?: Subscription;
  ContainerClass = 'container';
  activities?: ActivityArea[];
  states!: City[];
  submitted = false;
  enregister = false;
  tvaerror = false;
  selectedType: any;
  tvaerrorMsg = '';
  showModal = false;
  company = [
    { name: 'Fournisseur', compType: 'f' },
    { name: 'Prospect', compType: 'p' },
    { name: 'Client', compType: 'c' }
  ];

  compteForm = this.formBuilder.group({
    id: [''],
    compTva: ['', [Validators.required]],
    compSiret: [''],
    compRcs: [''],
    compSiren: [''],
    compSocForm: [''],
    compNbEmp: [''],
    infoName: ['', [Validators.required]],
    infoDesc: [''],
    compType: [[Validators.required]],
    compYearCre: [''],
    activity: []
  });

  constructor(
    protected personalinfoService: CompanyService,
    protected activatedRoute: ActivatedRoute,
    protected modifiercompteModalservice: ModifiercompteModalService,
    protected toastr: ToastrService,
    private formBuilder: FormBuilder,
    protected activityService: ActivityService,
    public activeModal: NgbActiveModal,
    protected eventManager: JhiEventManager
  ) {}

  ngOnInit(): void {
    this.loadAll();
    if (this.EditCompany !== undefined) {
      this.updateForm(this.EditCompany);
    }

    // this.compteForm = this.formBuilder.group({
    //   compTVA: ['', [Validators.required]],
    //   compSiret: [''],
    //   compRcs: [''],
    //   compSiren: [''],
    //   compSocForm: [''],
    //   compNbEmp: [''],
    //   infoName: ['', [Validators.required]],
    //   infoDesc: [''],
    //   compType: [[Validators.required]],
    //   compYearCre: [''],
    // });
  }

  show(): void {
    this.modifiercompteModalservice.open();
  }

  hide(): void {
    this.showModal = false;
    this.ContainerClass = 'container';
  }

  previousState(): void {
    window.history.back();
  }

  // convenience getter for easy access to form fields
  // eslint-disable-next-line @typescript-eslint/tslint/config
  get f() {
    return this.compteForm.controls;
  }

  save(): void {
    this.submitted = true;

    const company = this.createFromForm();

    if (this.submitted) {
      this.subscribeToSaveResponse(this.personalinfoService.update(company));
    }
  }

  updateForm(EditCompany: Company): void {
    this.compteForm.patchValue({
      id: EditCompany.id,
      compTva: EditCompany.compTva,
      compSiret: EditCompany.compSiret,
      compRcs: EditCompany.compRcs,
      compSiren: EditCompany.compSiren,
      compSocForm: EditCompany.compSocForm,
      compNbEmp: EditCompany.compNbEmp,
      infoName: EditCompany.infoName,
      infoDesc: EditCompany.infoDesc,
      compType: EditCompany.compType,
      compYearCre: EditCompany.compYearCre
    });
  }

  private createFromForm(): Company {
    return {
      ...new CCompany(),
      id: this.EditCompany?.id,
      infoName: this.compteForm.get(['infoName'])!.value,

      compTva: this.compteForm.get(['compTva'])!.value,
      compSiret: this.compteForm.get(['compSiret'])!.value,
      compSiren: this.compteForm.get(['compSiren'])!.value,
      compRcs: this.compteForm.get(['compRcs'])!.value,
      compSocForm: this.compteForm.get(['compSocForm'])!.value,
      compNbEmp: this.compteForm.get(['compNbEmp'])!.value,
      compType: this.compteForm.get(['compType'])!.value,
      compYearCre: this.compteForm.get(['compYearCre'])!.value,
      infoDesc: this.compteForm.get(['infoDesc'])!.value,
      deleted: 0
    };
  }

  protected subscribeToSaveResponse(result: Observable<HttpResponse<Company>>): void {
    result.subscribe(
      () => this.onSaveSuccess(),
      () => this.onSaveError(),
      () => this.eventManager.broadcast('modifCompte')
    );
  }

  protected onSaveSuccess(): void {
    this.submitted = false;
    this.compteForm.reset();
    this.compteForm.markAsPristine();
    if (this.enregister) {
      this.activeModal.close();
      this.enregister = false;
      this.ContainerClass = 'container';
    }
    this.toastr.success('Compte modifié');
    this.eventManager.broadcast('modifCompte');
  }

  protected onSaveError(): void {
    this.toastr.error('Compte existe deja');
    this.tvaerror = !this.tvaerror;
  }

  onClickMe(): void {
    this.enregister = true;
  }

  loadAll(): void {
    this.activityService.query().subscribe((res: HttpResponse<ActivityArea[]>) => (this.activities = res.body || []));
  }
}
