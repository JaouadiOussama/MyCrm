import { Component, OnInit } from '@angular/core';
import { Validators, FormBuilder, FormGroup } from '@angular/forms';
import { PersonalInfo, Email, Phone, Country, Company, City, ActivityArea, Contact, Adress } from 'app/shared/services/model';
import { HttpResponse } from '@angular/common/http';
import { Observable, Subscription } from 'rxjs';
import { CountryService } from 'app/shared/services/country.service';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { CompanyService } from 'app/shared/services/company.service';
import { ToastrService } from 'ngx-toastr';
import { AjoutcompteModalService } from 'app/shared/services/ajoutcompte-modal.service';
import { ActivityService } from 'app/shared/services/activity.service';
import { JhiEventManager } from 'ng-jhipster';

@Component({
  selector: 'jhi-ajout-compte-modal',
  templateUrl: './compte-ajouter.component.html',
  styleUrls: ['./compte-ajouter.component.scss']
})
export class AjoutcompteModalComponent implements OnInit {
  companies?: Company[];
  enventSubscriber?: Subscription;
  compteForm!: FormGroup;
  eventSubscriber?: Subscription;
  ContainerClass = 'container';
  activities?: ActivityArea[];
  countries: Country[] = [];
  states!: City[];
  submitted = false;
  selectedCity: any;
  country!: Country;
  enregister = false;
  isSaving = false;
  tvaerror = false;
  selectedType: any;
  tvaerrorMsg = '';
  showModal = false;
  company = [
    { name: 'Fournisseur', compType: 'Fournisseur' },
    { name: 'Prospect', compType: 'Prospect' },
    { name: 'Client', compType: 'Client' }
  ];

  constructor(
    protected personalinfoService: CompanyService,
    protected countryService: CountryService,
    protected activityService: ActivityService,
    protected ajoutcompteModalservice: AjoutcompteModalService,
    protected toastr: ToastrService,
    private formBuilder: FormBuilder,
    public activeModal: NgbActiveModal,
    protected eventManager: JhiEventManager
  ) {}

  ngOnInit(): void {
    this.loadAll();

    this.compteForm = this.formBuilder.group({
      compTva: ['', [Validators.required]],
      infoName: ['', [Validators.required]],
      infoDesc: [''],
      compType: [[Validators.required]],
      codeZip: [''],
      adr1: [''],
      adr2: [''],
      // LastName: [''],
      //name: [''],
      email: ['', [Validators.email]],
      pays: [''],
      ville: [null],
      activity: [null],
      phone: ['', [Validators.pattern(/^-?([0-9]\d*)?$/)]]
    });
  }

  show(): void {
    this.ajoutcompteModalservice.open();
  }

  hide(): void {
    this.showModal = false;
    this.ContainerClass = 'container';
  }

  loadAll(): void {
    this.countryService
      .query({
        size: 248
      })
      .subscribe((res: HttpResponse<Country[]>) => (this.countries = res.body || []));
    this.activityService.query().subscribe((res: HttpResponse<ActivityArea[]>) => (this.activities = res.body || []));
  }

  onSelect(): void {
    this.compteForm.get(['ville'])?.reset();
    this.compteForm.get(['phone'])?.reset();
    if (this.selectedCity !== null) {
      this.countryService.findCities(this.selectedCity).subscribe((res: HttpResponse<any>) => (this.states = res.body || []));
      this.countryService.find(this.selectedCity).subscribe((res: HttpResponse<any>) => (this.country = res.body || []));
    }
  }

  previousState(): void {
    window.history.back();
  }

  // convenience getter for easy access to form fields
  // eslint-disable-next-line @typescript-eslint/tslint/config
  get f() {
    return this.compteForm.controls;
  }

  save(): void {
    this.submitted = true;

    const company = this.createCompany();
    // stop here if form is invalid
    if (this.compteForm.invalid) {
      return;
    }
    if (this.submitted) {
      this.subscribeToSaveResponse(this.personalinfoService.create(company));
    }
  }

  private createCompany(): Company {
    return {
      infoName: this.compteForm.get(['infoName'])!.value,
      compTva: this.compteForm.get(['compTva'])!.value,
      infoDesc: this.compteForm.get(['infoDesc'])!.value,
      compType: this.compteForm.get(['compType'])!.value,
      adresses: this.createAdressCompany(),
      emails: this.createEmail(),
      phones: this.createPhone(),
      //people: this.createContact(),
      activityarea: this.createActivity(),
      deleted: 0
    };
  }

  /* private createContact(): Contact[] {
    return [
      {
        infoName: this.compteForm.get(['name'])!.value,
        persLastName: this.compteForm.get(['LastName'])!.value,
        personalInfos: this.createPersonInfocontact()
      }
    ];
  }
 */
  private createAdressCompany(): Adress[] {
    return [
      {
        adr1: this.compteForm.get(['adr1'])!.value,
        adr2: this.compteForm.get(['adr2'])!.value,
        codeZip: this.compteForm.get(['codeZip'])!.value,
        city: this.createCity()
      }
    ];
  }

  /*   private createPersonInfocontact(): PersonalInfo[] {
    return [
      {
        emails: this.createEmail(),
        phones: this.createPhone()
      }
    ];
  } */

  private createEmail(): Email[] {
    return [
      {
        email: this.compteForm.get(['email'])!.value
      }
    ];
  }

  private createPhone(): Phone[] {
    return [
      {
        phoneNumber: this.compteForm.get(['phone'])!.value
      }
    ];
  }

  /* private createCountry(): Country {
    return {
      id: this.compteForm.get(['pays'])!.value,
      cities: this.createCity()
    };
  } */

  private createCity(): City {
    return { id: this.compteForm.get(['ville'])!.value };
  }

  private createActivity(): ActivityArea {
    return {
      id: this.compteForm.get(['activity'])!.value
    };
  }

  protected subscribeToSaveResponse(result: Observable<HttpResponse<Company>>): void {
    result.subscribe(
      () => this.onSaveSuccess(),
      () => this.onSaveError(),
      () => this.eventManager.broadcast('modifCompte')
    );
  }

  protected onSaveSuccess(): void {
    this.submitted = false;
    this.compteForm.reset();
    this.compteForm.markAsPristine();
    if (this.enregister) {
      this.activeModal.close();
      this.enregister = false;
      this.ContainerClass = 'container';
    }
    this.toastr.success('Compte créé avec succès');
    this.eventManager.broadcast('modifCompte');
  }

  protected onSaveError(): void {
    this.toastr.error('Compte existe deja');
    this.tvaerror = !this.tvaerror;
  }

  onClickMe(): void {
    this.enregister = true;
  }
}
