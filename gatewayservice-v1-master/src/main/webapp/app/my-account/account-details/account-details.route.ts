import { Route } from '@angular/router';

import { AccountDetailsComponent } from 'app/my-account/account-details/account-details.component';

export const accountdetailsRoute: Route = {
  path: 'accountdetails/:id',
  component: AccountDetailsComponent
  /* data: {
    authorities: [],
    pageTitle: 'accountlist.title'
  }*/
};
