import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { CompanyService } from 'app/shared/services/company.service';
import { HttpResponse } from '@angular/common/http';
import { ModifiercompteModalService } from 'app/shared/services/modifiercompte-modal.service';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { ModifiercompteModalComponent } from 'app/my-account/account-modifier/account-modifier.component';
import { JhiEventManager } from 'ng-jhipster';
import { Subscription } from 'rxjs';

import { Company, Phone, PersonalInfo, Profil } from 'app/shared/services/model';
import { PhoneService } from 'app/shared/services/phone.service';
import { ProfilService } from 'app/shared/services/profil.service';

@Component({
  selector: 'jhi-account-details',
  templateUrl: './account-details.component.html',
  styleUrls: ['./account-details.component.scss']
})
export class AccountDetailsComponent implements OnInit {
  id: any;
  company: any;
  resume = false;
  coordonne = true;
  personalInfos: PersonalInfo[] = [];
  personalInfoId = 0;
  companyId = 0;
  phones: Phone[] = [];
  listPhones: Phone[] = [];
  profils?: Profil[];
  listefaceBook?: Profil[];
  facebookUrl: any;
  listeTwitter?: Profil[];
  twitterUrl: any;
  listeLindeIn?: Profil[];
  linkedInUrl: any;
  buttonColorResume = '#cecece';
  buttonColorCoordonne = '';

  enventSubscriber?: Subscription;

  items = [
    { id: 1, name: 'Python' },
    { id: 2, name: 'Node Js' },
    { id: 3, name: 'Java' },
    { id: 4, name: 'PHP', disabled: true },
    { id: 5, name: 'Django' },
    { id: 6, name: 'Angular' },
    { id: 7, name: 'Vue' },
    { id: 8, name: 'ReactJs' }
  ];
  selected = [
    { id: 2, name: 'Node Js' },
    { id: 8, name: 'ReactJs' }
  ];

  constructor(
    protected profilService: ProfilService,
    private route: ActivatedRoute,
    protected personalinfoService: CompanyService,
    protected modalService: NgbModal,
    protected eventManager: JhiEventManager,
    protected modifiercompteModalservice: ModifiercompteModalService,
    protected phoneService: PhoneService,
    protected companyService: CompanyService
  ) {}

  ngOnInit(): void {
    this.id = this.route.snapshot.params['id'];
    this.loadAll();
    this.getById();
    this.registerChangeCompte();
  }

  /* getById(): void {
    this.personalinfoService.find(this.id).subscribe((res: HttpResponse<Company>) => (this.company = res.body || []));
    this.personalInfos = this.company.personalInfos || [];
  } */

  getById(): void {
    this.companyService.find(this.id).subscribe((res: HttpResponse<Company>) => (this.company = res.body || []));
  }
  displayResume(): void {
    this.resume = false;
    this.coordonne = true;
    this.buttonColorCoordonne = ''; // desired Color
    this.buttonColorResume = '#cecece';
  }
  displayCoordonne(): void {
    this.coordonne = false;
    this.resume = true;
    this.buttonColorCoordonne = '#cecece'; // desired Color
    this.buttonColorResume = '';
  }

  getListPhones(): void {
    this.phoneService.findByDeletedAndInfo(0, 1055).subscribe(res => {
      this.listPhones = res.body || [];
    });
  }

  // open pop up
  show(): void {
    const modalRef = this.modalService.open(ModifiercompteModalComponent, { size: 'md', backdrop: 'static' });
    modalRef.componentInstance.EditCompany = this.company;
  }
  registerChangeCompte(): void {
    this.enventSubscriber = this.eventManager.subscribe('modifCompte', () => this.reload());
  }

  reload(): void {
    this.getById();
  }

  loadAll(): void {
    this.profilService.findByDeletedAndPersonalInfo(0, this.id).subscribe((res: HttpResponse<Profil[]>) => {
      this.profils = res.body || [];
      this.listefaceBook = this.profils.filter(b => b.profil?.toLowerCase().includes('Facebook'.toLowerCase()));
      this.facebookUrl = this.listefaceBook[0] ? this.listefaceBook[0] : '#';
      this.listeTwitter = this.profils.filter(b => b.profil?.toLowerCase().includes('Twitter'.toLowerCase()));
      this.twitterUrl = this.listeTwitter[0] ? this.listeTwitter[0] : '#';
      this.listeLindeIn = this.profils.filter(b => b.profil?.toLowerCase().includes('LinkedIn'.toLowerCase()));
      this.linkedInUrl = this.listeLindeIn[0] ? this.listeLindeIn[0] : '#';
    });
  }
}
