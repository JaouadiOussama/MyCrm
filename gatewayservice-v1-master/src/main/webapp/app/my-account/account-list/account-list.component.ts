import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';

import { ToastrService } from 'ngx-toastr';
import { AjoutcompteModalService } from 'app/shared/services/ajoutcompte-modal.service';
import { Component, OnDestroy, OnInit } from '@angular/core';
import { Company } from 'app/shared/services/model';
import { Subscription } from 'rxjs';
import { CompanyService } from 'app/shared/services/company.service';
import { JhiEventManager } from 'ng-jhipster';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { HttpResponse, HttpHeaders } from '@angular/common/http';
import { AccountDeleteComponent } from '../account-delete/account-delete.component';
import { AngularCsv } from 'angular7-csv/dist/Angular-csv';

@Component({
  selector: 'jhi-account-list',
  templateUrl: './account-list.component.html',
  styleUrls: ['./account-list.component.scss']
})
export class AccountListComponent implements OnInit, OnDestroy {
  companies: Company[] = [];
  companiesCsv?: Company[];
  NamesAscending = true;
  enventSubscriber?: Subscription;

  checkedClicked = false;
  allButtonChecked = false;

  /* deleted */
  toBeDeleted: number[] = [];
  toBeDeletedApi: number[] = [];
  csvOptions = {
    fieldSeparator: ',',
    quoteStrings: '"',
    decimalseparator: '.',
    showLabels: true,
    showTitle: true,
    title: 'Your account List :',
    useBom: true,
    noDownload: false,
    headers: ['Nom', 'Type', 'Adresse postale', 'Téléphone', 'Courriel']
  };

  /* pagination */
  page = 1;
  pageSize = 10;
  totalItems = 0;

  constructor(
    protected personalinfoService: CompanyService,
    // protected eventManager: JhiEventManager,
    protected toastr: ToastrService,
    public activeModal: NgbActiveModal,
    protected ajoutcompteModalservice: AjoutcompteModalService,
    protected eventManager: JhiEventManager,
    protected modalService: NgbModal
  ) {}

  loadAll(): void {
    this.personalinfoService.findByDeleted(0).subscribe((res: HttpResponse<Company[]>) => this.onSuccess(res.body, res.headers));
  }

  private onSuccess(companies: Company[] | null, headers: HttpHeaders): void {
    this.totalItems = Number(headers.get('X-Total-Count'));
    this.companies = companies || [];
  }

  ngOnInit(): void {
    this.loadAll();
    this.registerChangeCompte();
    if (!this.toBeDeletedApi.length) {
      this.toBeDeleted = [];
    }
  }
  ngOnDestroy(): void {
    if (this.enventSubscriber) {
      this.eventManager.destroy(this.enventSubscriber);
    }
  }

  reload(): void {
    // this.companies = this.companies?.filter((currentValue, index) => !this.toBeDeleted.includes(index));
    this.loadAll();
    this.companies = this.companies?.filter((currentValue, index) => !this.toBeDeleted.includes(index));
    this.toBeDeleted = [];
    this.checkedClicked = false;
    this.allButtonChecked = false;
  }

  registerChangeCompte(): void {
    this.enventSubscriber = this.eventManager.subscribe('modifCompte', () => this.reload());
  }

  trackId(index: number, item: Company): number {
    // eslint-disable-next-line @typescript-eslint/no-unnecessary-type-assertion
    return item.id!;
  }

  checkAll(event: any): void {
    if (event.target.checked) {
      this.checkedClicked = true;
      this.allButtonChecked = true;
      for (let i = 0; i < this.companies.length; i++) {
        this.toBeDeleted.push(i);
        //console.log(this.toBeDeleted)
      }
    } else {
      this.checkedClicked = false;
      this.allButtonChecked = false;
      this.toBeDeleted = [];
    }
  }

  checkOne(): void {
    //  this.checkedClicked = !this.checkedClicked;
    if (!this.toBeDeleted.length && !this.allButtonChecked) {
      this.checkedClicked = false;
    } else {
      this.checkedClicked = true;
    }
  }
  delete(): void {
    const modalRef = this.modalService.open(AccountDeleteComponent, { size: 'md', backdrop: 'static' });
    modalRef.componentInstance.toBeDeletedApi = this.toBeDeletedApi;
    this.toBeDeletedApi = [];
  }
  remove(event: any, index: any, indexApi: number): void {
    if (event.target.checked) {
      this.toBeDeleted.push(index);
      this.toBeDeletedApi.push(indexApi);
    } else {
      const idx = this.toBeDeleted.indexOf(index);
      this.toBeDeleted.splice(idx, 1);
      const idxApi = this.toBeDeletedApi.indexOf(indexApi);
      this.toBeDeletedApi.splice(idxApi, 1);
    }
  }

  // open pop up
  show(): void {
    this.ajoutcompteModalservice.open();
  }
  downloadCSV(): any {
    // this.dtHolidays : JSONDATA , HolidayList : CSV file Name, this.csvOptions : file options

    if (this.toBeDeleted.length) {
      this.companiesCsv = this.companies?.filter((currentValue, index) => this.toBeDeleted.includes(index));
      // eslint-disable-next-line no-new
      new AngularCsv(this.companiesCsv, 'AccountList', this.csvOptions);
    }
  }
  SortName(): void {
    this.companies = this.companies.sort((a, b) =>
      (a.infoName ? a.infoName : '') < (b.infoName ? b.infoName : '') ? (this.NamesAscending ? -1 : 1) : this.NamesAscending ? 1 : -1
    );
  }

  /* pagination */
}
