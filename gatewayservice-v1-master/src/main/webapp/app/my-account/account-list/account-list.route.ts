import { Route } from '@angular/router';
import { AccountListComponent } from 'app/my-account/account-list/account-list.component';

export const accountListRoute: Route = {
  path: 'accountlist',
  component: AccountListComponent
  /* data: {
    authorities: [],
    pageTitle: 'accountlist.title'
  }*/
};
