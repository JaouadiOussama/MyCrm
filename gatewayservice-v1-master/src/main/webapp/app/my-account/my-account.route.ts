import { Routes } from '@angular/router';
import { accountListRoute } from 'app/my-account/account-list/account-list.route';
import { accountdetailsRoute } from 'app/my-account/account-details/account-details.route';
import { Company, CCompany } from 'app/shared/services/model';
import { Resolve, Router, ActivatedRouteSnapshot } from '@angular/router';
import { CompanyService } from 'app/shared/services/company.service';
import { Injectable } from '@angular/core';
import { Observable, of, EMPTY } from 'rxjs';
import { flatMap } from 'rxjs/operators';
import { HttpResponse } from '@angular/common/http';
import { Authority } from 'app/shared/constants/authority.constants';
import { UserRouteAccessService } from 'app/core/auth/user-route-access-service';
import { ModifiercompteModalComponent } from 'app/my-account/account-modifier/account-modifier.component';

const MY_ACCOUNT_ROUTES = [accountListRoute, accountdetailsRoute];

@Injectable({ providedIn: 'root' })
export class CompanyResolve implements Resolve<Company> {
  constructor(private service: CompanyService, private router: Router) {}

  resolve(route: ActivatedRouteSnapshot): Observable<Company> | Observable<never> {
    const id = route.params['id'];
    if (id) {
      return this.service.find(id).pipe(
        flatMap((company: HttpResponse<Company>) => {
          if (company.body) {
            return of(company.body);
          } else {
            this.router.navigate(['404']);
            return EMPTY;
          }
        })
      );
    }
    return of(new CCompany());
  }
}
export const MyAccountState: Routes = [
  {
    path: '',
    children: MY_ACCOUNT_ROUTES
  },
  {
    path: ':id/edit',
    component: ModifiercompteModalComponent,
    resolve: {
      company: CompanyResolve
    },
    data: {
      authorities: [Authority.USER],
      pageTitle: 'company'
    },
    canActivate: [UserRouteAccessService]
  }
];
