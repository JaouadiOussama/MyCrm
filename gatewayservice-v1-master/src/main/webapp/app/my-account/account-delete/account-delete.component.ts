import { Component } from '@angular/core';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { Company, CCompany } from 'app/shared/services/model';

import { CompanyService } from 'app/shared/services/company.service';

import { HttpResponse } from '@angular/common/http';
import { ToastrService } from 'ngx-toastr';
import { JhiEventManager } from 'ng-jhipster';

@Component({
  selector: 'jhi-account-delete',
  templateUrl: './account-delete.component.html'
})
export class AccountDeleteComponent {
  company?: Company;
  toBeDeletedApi: number[] = [];

  // toBeDeleted: number[] = [];
  deleted = false;

  constructor(
    protected companyService: CompanyService,
    public activeModal: NgbActiveModal,
    protected toastr: ToastrService,
    protected eventManager: JhiEventManager
  ) {}

  cancel(): void {
    this.activeModal.dismiss();
    //  this.toBeDeleted=[];
  }
  confirmDelete(): void {
    for (let i = 0; i < this.toBeDeletedApi.length; i++) {
      this.companyService.find(this.toBeDeletedApi[i]).subscribe((res: HttpResponse<Company>) => {
        this.company = res.body || new CCompany();
        this.company.deleted = 1;
        this.companyService.update(this.company).subscribe(() => {
          this.eventManager.broadcast('modifCompte');
        });
      });
    }

    if (this.toBeDeletedApi.length === 1) {
      this.toastr.success('Compte supprimé avec succès');
    } else {
      this.toastr.success(this.toBeDeletedApi.length + ' comptes supprimés avec succès');
    }
    this.deleted = true;
    this.activeModal.close(this.deleted);
  }
}
