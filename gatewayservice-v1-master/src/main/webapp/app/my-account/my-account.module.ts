import { NgModule } from '@angular/core';

import { AccountListComponent } from './account-list/account-list.component';

import { GatewayServiceSharedModule } from 'app/shared/shared.module';
import { RouterModule } from '@angular/router';
import { MyAccountState } from 'app/my-account/my-account.route';
import { AccountDetailsComponent } from './account-details/account-details.component';
import { AccountSidebarComponent } from './account-sidebar/account-sidebar.component';
import { NgSelectModule } from '@ng-select/ng-select';
import { AjoutcompteModalComponent } from './compte-ajouter/compte-ajouter.component';
import { ModifiercompteModalComponent } from './account-modifier/account-modifier.component';
import { ReactiveFormsModule } from '@angular/forms';
import { AccountDeleteComponent } from './account-delete/account-delete.component';

@NgModule({
  imports: [GatewayServiceSharedModule, ReactiveFormsModule, RouterModule.forChild(MyAccountState), NgSelectModule],
  declarations: [
    AccountListComponent,
    AccountDetailsComponent,
    AccountSidebarComponent,
    AjoutcompteModalComponent,
    ModifiercompteModalComponent,
    AccountDeleteComponent
  ],

  entryComponents: [AccountDeleteComponent]
})
export class MyAccountModule {}
