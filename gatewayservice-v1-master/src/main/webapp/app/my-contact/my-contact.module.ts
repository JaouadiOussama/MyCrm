import { NgModule } from '@angular/core';

import { GatewayServiceSharedModule } from 'app/shared/shared.module';
import { RouterModule } from '@angular/router';
import { ContactListComponent } from './contact-list/contact-list.component';
import { MyContactState } from './my-contact.route';
import { NgSelectModule } from '@ng-select/ng-select';
import { AjoutcontactModalComponent } from './contact-ajouter/contact-ajouter.component';
import { ContactDeleteComponent } from './contact-delete/contact-delete.component';
import { CustomPaginationComponent } from 'app/shared/pagination/custom-pagination.component';
import { ModifiercontactModalComponent } from './contact-modifier/contact-modifier.comonent';
import { ContactDetailsComponent } from './contact-details/contact-details.component';

@NgModule({
  imports: [GatewayServiceSharedModule, NgSelectModule, RouterModule.forChild(MyContactState)],
  declarations: [
    ContactListComponent,
    AjoutcontactModalComponent,
    ContactDeleteComponent,
    CustomPaginationComponent,
    ContactDetailsComponent,
    ModifiercontactModalComponent
  ],
  entryComponents: [ContactDeleteComponent]
})
export class MyContactModule {}
