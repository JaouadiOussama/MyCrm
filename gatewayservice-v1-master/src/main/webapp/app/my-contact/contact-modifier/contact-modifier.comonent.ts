import { Component, OnInit, OnDestroy } from '@angular/core';
import { Contact, CContact, Company, Person } from 'app/shared/services/model';
import { ContactService } from 'app/shared/services/my-contact.service';
import { ActivatedRoute } from '@angular/router';
import { FormBuilder, Validators } from '@angular/forms';
import { ToastrService } from 'ngx-toastr';
import { JhiEventManager, JhiEventWithContent } from 'ng-jhipster';
import { TranslateService } from '@ngx-translate/core';
import { NgbActiveModal, NgbDateStruct } from '@ng-bootstrap/ng-bootstrap';
import { Subscription, Observable } from 'rxjs';
import { HttpErrorResponse, HttpResponse } from '@angular/common/http';
import { CompanyService } from 'app/shared/services/company.service';

@Component({
  selector: 'jhi-contact-modifier-modal',
  templateUrl: './contact-modifier.component.html',
  styleUrls: ['./contact-modifier.component.scss']
})
export class ModifiercontactModalComponent implements OnInit, OnDestroy {
  isSaving = false;
  contact?: Contact;

  infoId = 0;
  codeContact = 0;
  selectedCompany: any;
  model!: NgbDateStruct;

  companies?: Company[];

  httpErrorListener!: Subscription;

  originProspects = [{ name: 'Séminaires' }];

  readonly DELIMITER = '-';

  editForm = this.fb.group({
    id: [],
    infoName: ['', [Validators.required, Validators.pattern('^[a-zA-Z ]*$')]],
    persLastName: ['', [Validators.required, Validators.pattern('^[a-zA-Z ]*$')]],
    persFonction: ['', Validators.pattern('^[a-zA-Z ]*$')],
    companyName: [],
    personBirthDate: [],
    persDepartement: [],
    contactLeadOrigin: []
  });
  constructor(
    protected contactService: ContactService,
    protected activatedRoute: ActivatedRoute,
    private fb: FormBuilder,
    protected toastr: ToastrService,
    protected eventManager: JhiEventManager,
    public activeModal: NgbActiveModal,
    public translateService: TranslateService,
    protected companyService: CompanyService
  ) {
    this.httpErrorListener = this.eventManager.subscribe(
      'gatewayServiceApp.httpError',
      (response: JhiEventWithContent<HttpErrorResponse>) => {
        const httpErrorResponse = response.content;
        const entityNameUniqueUrl = this.translateService.instant('contact.message.uniqueUrl');

        switch (httpErrorResponse.status) {
          case 400:
            this.toastr.error(entityNameUniqueUrl);

            break;
        }
      }
    );
  }

  cancel(): void {
    this.activeModal.dismiss();
  }
  ngOnInit(): void {
    if (this.contact !== undefined) {
      this.updateForm(this.contact);
      this.companyService.findByDeleted(0).subscribe((res: HttpResponse<Company[]>) => (this.companies = res.body || []));
    }
  }
  updateForm(contact: Contact): void {
    this.editForm.patchValue({
      id: contact.id,
      infoName: contact.infoName,
      persLastName: contact.persLastName,
      persFonction: contact.perFonction,
      companyName: contact.company?.id,
      personBirthDate: contact.persBirthDate,
      persDepartement: contact.persDepartement,
      contactLeadOrigin: contact.contactLeadOrigin
    });
  }

  save(): void {
    this.isSaving = true;
    const contact = this.createFromForm();

    this.subscribeToSaveResponse(this.contactService.update(contact));
  }
  private createFromForm(): Contact {
    const contact = new Contact();
    contact.id = this.editForm.get(['id'])!.value;
    contact.infoName =
      this.editForm
        .get(['infoName'])!
        .value.charAt(0)
        .toUpperCase() + this.editForm.get(['infoName'])!.value.slice(1);
    contact.persLastName =
      this.editForm
        .get(['persLastName'])!
        .value.charAt(0)
        .toUpperCase() + this.editForm.get(['persLastName'])!.value.slice(1);
    contact.perFonction = this.editForm.get(['persFonction'])!.value;
    contact.persDepartement = this.editForm.get(['persDepartement'])!.value;
    if (this.editForm.get(['companyName'])!.value) {
      contact.company = this.createCompte();
    }

    contact.persBirthDate = this.editForm.get(['personBirthDate'])!.value;
    contact.contactLeadOrigin = this.editForm.get(['contactLeadOrigin'])!.value;
    contact.deleted = 0;

    return contact;
  }
  protected subscribeToSaveResponse(result: Observable<HttpResponse<Contact>>): void {
    result.subscribe(
      () => this.onSaveSuccess(),
      () => this.onSaveError()
    );
  }

  protected onSaveSuccess(): void {
    this.isSaving = false;

    const entityNameUpdate = this.translateService.instant('contact.message.update');

    this.toastr.success(entityNameUpdate);

    this.eventManager.broadcast('test');
    this.activeModal.close();
  }

  protected onSaveError(): void {
    this.isSaving = false;
  }

  ngOnDestroy(): void {
    if (this.httpErrorListener) {
      this.eventManager.destroy(this.httpErrorListener);
    }
  }

  private createCompte(): Person {
    return {
      id: this.editForm.get(['companyName'])!.value
    };
  }

  fromModel(value: string | null): NgbDateStruct | null {
    if (value) {
      const date = value.split(this.DELIMITER);
      return {
        year: parseInt(date[0], 10),
        month: parseInt(date[1], 10),
        day: parseInt(date[2], 10)
      };
    }
    return null;
  }

  toModel(date: NgbDateStruct | null): string | null {
    return date ? date.day + this.DELIMITER + date.month + this.DELIMITER + date.year : null;
  }
}
