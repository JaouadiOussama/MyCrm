import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { ContactService } from 'app/shared/services/my-contact.service';
import { HttpResponse } from '@angular/common/http';
import { Contact, PersonalInfo, Phone, Profil } from 'app/shared/services/model';
import { ModifiercontactModalComponent } from '../contact-modifier/contact-modifier.comonent';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { ProfilService } from 'app/shared/services/profil.service';
import { JhiEventManager } from 'ng-jhipster';
import { PhoneService } from 'app/shared/services/phone.service';
import { Subscription } from 'rxjs';

@Component({
  selector: 'jhi-contact-details',
  templateUrl: './contact-details.component.html',
  styleUrls: ['./contact-details.component.scss']
})
export class ContactDetailsComponent implements OnInit, OnDestroy {
  id: any;
  contact: any;

  resume = false;
  coordonne = true;

  personalInfos: PersonalInfo[] = [];
  personalInfoId = 0;
  phones: Phone[] = [];
  listPhones: Phone[] = [];
  profils?: Profil[];
  listefaceBook?: Profil[];
  facebookUrl: any;
  listeTwitter?: Profil[];
  twitterUrl: any;
  listeLindeIn?: Profil[];
  linkedInUrl: any;

  enventSubscriber?: Subscription;

  buttonColorResume = '#cecece';
  buttonColorCoordonne = '';

  constructor(
    private route: ActivatedRoute,
    protected contactService: ContactService,
    public modalService: NgbModal,
    protected profilService: ProfilService,
    protected eventManager: JhiEventManager,
    protected phoneService: PhoneService
  ) {}

  ngOnInit(): void {
    this.id = this.route.snapshot.params['id'];
    this.registerChangeCompte();
    this.loadAll();
    this.getById();
  }

  getById(): void {
    this.contactService.find(this.id).subscribe((res: HttpResponse<Contact>) => (this.contact = res.body || []));
    this.personalInfos = this.contact.personalInfos || [];
  }
  /**
   * update contact
   */
  update(contact?: Contact): void {
    const modalRef = this.modalService.open(ModifiercontactModalComponent, { size: 'md', backdrop: 'static' });
    modalRef.componentInstance.contact = contact;
    // modalRef.componentInstance.infoId = this.infoId;
    //   modalRef.componentInstance.codeContact = this.country?.codeContact;
    if (contact) {
      modalRef.componentInstance.create = false;
    } else {
      modalRef.componentInstance.create = true;
    }
  }

  displayResume(): void {
    this.resume = false;
    this.coordonne = true;
    this.buttonColorCoordonne = ''; //  desired Color
    this.buttonColorResume = '#cecece';
  }
  displayCoordonne(): void {
    this.coordonne = false;
    this.resume = true;
    this.buttonColorCoordonne = '#cecece'; // desired Color
    this.buttonColorResume = '';
  }

  registerChangeCompte(): void {
    this.enventSubscriber = this.eventManager.subscribe('test', () => this.reload());
  }

  reload(): void {
    this.getById();
  }

  loadAll(): void {
    this.profilService.findByDeletedAndPersonalInfo(0, this.id).subscribe((res: HttpResponse<Profil[]>) => {
      this.profils = res.body || [];
      this.listefaceBook = this.profils.filter(b => b.profil?.toLowerCase().includes('Facebook'.toLowerCase()));
      this.facebookUrl = this.listefaceBook[0] ? this.listefaceBook[0] : '#';
      this.listeTwitter = this.profils.filter(b => b.profil?.toLowerCase().includes('Twitter'.toLowerCase()));
      this.twitterUrl = this.listeTwitter[0] ? this.listeTwitter[0] : '#';
      this.listeLindeIn = this.profils.filter(b => b.profil?.toLowerCase().includes('LinkedIn'.toLowerCase()));
      this.linkedInUrl = this.listeLindeIn[0] ? this.listeLindeIn[0] : '#';
    });
  }

  ngOnDestroy(): void {
    if (this.enventSubscriber) {
      this.eventManager.destroy(this.enventSubscriber);
    }
  }
}
