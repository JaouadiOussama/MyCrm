import { Route } from '@angular/router';
import { ContactDetailsComponent } from 'app/my-contact/contact-details/contact-details.component';

export const contactDetailsRoute: Route = {
  path: 'contactDetails/:id',
  component: ContactDetailsComponent
  /* data: {
    authorities: [],
    pageTitle: 'contactlist.title'
  }*/
};
