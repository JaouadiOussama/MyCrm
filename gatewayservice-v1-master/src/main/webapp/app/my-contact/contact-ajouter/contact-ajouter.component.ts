import { Component, OnInit } from '@angular/core';
import { Validators, FormBuilder, FormGroup } from '@angular/forms';
import { ContactService } from 'app/shared/services/my-contact.service';
import { Contact, PersonalInfo, Email, Phone, Country, Company, City, Adress, Person, CContact, AAdress } from 'app/shared/services/model';
import { HttpResponse } from '@angular/common/http';
import { Observable } from 'rxjs';
import { CountryService } from 'app/shared/services/country.service';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { JhiParseLinks, JhiEventManager } from 'ng-jhipster';
import { CompanyService } from 'app/shared/services/company.service';
import { ToastrService } from 'ngx-toastr';
import { AjoutcontactModalService } from 'app/shared/services/ajoutconact-modal.service';
import { EmailService } from 'app/shared/services/email.service';
import { AjoutcompteModalService } from 'app/shared/services/ajoutcompte-modal.service';
import { TranslateService } from '@ngx-translate/core';

@Component({
  selector: 'jhi-ajout-contact-modal',
  templateUrl: './contact-ajouter.component.html',
  styleUrls: ['./contact-ajouter.component.scss']
})
export class AjoutcontactModalComponent implements OnInit {
  showModal = false;
  registerForm!: FormGroup;
  enregister = false;

  submitted = false;
  contacts: any[] = [
    { nom: 'contact1', type: 'contact1', adr: 'adresse', tel: '45125', courriel: 'aa@gmail.com' },
    { nom: 'contact2', type: 'contact2', adr: 'adresse', tel: '745125', courriel: 'aa@gmail.com' }
  ];

  countries: Country[] = [];
  country!: Country;
  test: any;
  states!: City[];
  selectedCity: any;
  selectedCompany: any;

  companies?: Company[];
  genres = ['Mr.', 'Ms.', 'Mrs.', 'Dr.', 'Prof.'];

  emailerror = false;
  emailerrorMsg = '';
  ContainerClass = 'container';

  constructor(
    protected contactService: ContactService,
    protected countryService: CountryService,
    private formBuilder: FormBuilder,
    protected parseLinks: JhiParseLinks,
    protected personalinfoService: CompanyService,
    protected toastr: ToastrService,
    protected emailService: EmailService,
    protected ajoutcontactModalservice: AjoutcontactModalService,
    public activeModal: NgbActiveModal,
    protected ajoutcompteModalservice: AjoutcompteModalService,
    protected eventManager: JhiEventManager,
    protected translateService: TranslateService
  ) {}

  show(): void {
    /* this.showModal = true; // Show-Hide Modal Check
      this.ContainerClass = 'containerBlur';
      this.registerForm.reset();
      this.registerForm.markAsPristine();*/
    this.ajoutcontactModalservice.open();
  }

  // Bootstrap Modal Close event
  hide(): void {
    this.showModal = false;
    this.ContainerClass = 'container';
  }

  //  load countries list
  loadAll(): void {
    this.countryService
      .query({
        size: 248
      })
      .subscribe((res: HttpResponse<Country[]>) => (this.countries = res.body || []));
  }
  onSelect(): void {
    this.registerForm.get(['ville'])?.reset();
    if (this.selectedCity !== null) {
      this.countryService.findCities(this.selectedCity).subscribe((res: HttpResponse<any>) => (this.states = res.body || []));
      this.countryService.find(this.selectedCity).subscribe((res: HttpResponse<any>) => (this.country = res.body || []));
    }

    this.test = 'tst';
  }

  ngOnInit(): void {
    this.loadAll();
    this.personalinfoService.findByDeleted(0).subscribe((res: HttpResponse<Company[]>) => (this.companies = res.body || []));

    this.registerForm = this.formBuilder.group({
      infoName: ['', [Validators.required, Validators.pattern('^[a-zA-Zéçàè ]*$')]],
      persLastName: ['', [Validators.required, Validators.pattern('^[a-zA-Zéçàè ]*$')]],
      persFonction: ['', Validators.pattern('^[a-zA-Z ]*$')],
      genre: ['Mr.'],
      compte: [],
      infoDesc: [],
      adresse: [],
      adresseC: [],
      codePostale: [],
      pays: [],
      ville: [],
      email: ['', [Validators.required, Validators.email]],
      mobile: ['', [Validators.pattern(/^-?([0-9]\d*)?$/)]],
      codePhone: []
    });
  }

  previousState(): void {
    window.location.reload();
  }

  // convenience getter for easy access to form fields
  // eslint-disable-next-line @typescript-eslint/tslint/config
  get f() {
    return this.registerForm.controls;
  }
  onSubmit(): void {
    this.submitted = true;
    const contact = this.createFromForm();
    // stop here if form is invalid
    if (this.registerForm.invalid) {
      return;
    }
    if (this.submitted) {
      this.subscribeToSaveResponse(this.contactService.create(contact));
    }
  }
  private createFromForm(): Contact {
    const contact = new CContact();
    contact.infoName =
      this.registerForm
        .get(['infoName'])!
        .value.charAt(0)
        .toUpperCase() + this.registerForm.get(['infoName'])!.value.slice(1);
    contact.persLastName =
      this.registerForm
        .get(['persLastName'])!
        .value.charAt(0)
        .toUpperCase() + this.registerForm.get(['persLastName'])!.value.slice(1);
    contact.infoDesc = this.registerForm.get(['infoDesc'])!.value;
    contact.perFonction = this.registerForm.get(['persFonction'])!.value;
    contact.perssGender = this.registerForm.get(['genre'])!.value;
    contact.emails = this.createEmail();
    contact.phones = this.createPhone();
    contact.adresses = this.createAdresse();
    if (this.registerForm.get(['compte'])!.value) {
      contact.company = this.createCompte();
    }

    contact.deleted = 0;

    return contact;
  }
  private createAdresse(): Adress[] {
    const adresse = new AAdress();
    adresse.adr1 = this.registerForm.get(['adresse'])!.value;
    adresse.adr2 = this.registerForm.get(['adresseC'])!.value;
    adresse.codeZip = this.registerForm.get(['codePostale'])!.value;
    adresse.deleted = 0;
    if (this.registerForm.get(['ville'])!.value) {
      adresse.city = this.createCity();
    }

    return [adresse];
  }

  private createEmail(): Email[] {
    return [
      {
        email: this.registerForm.get(['email'])!.value,
        deleated: 0
      }
    ];
  }
  private createPhone(): Phone[] {
    return [
      {
        phoneNumber: this.registerForm.get(['mobile'])!.value,
        deleted: 0
      }
    ];
  }

  private createCompte(): Person {
    return {
      id: this.registerForm.get(['compte'])!.value
    };
  }

  private createCity(): City {
    return {
      id: this.registerForm.get(['ville'])!.value
    };
  }

  protected subscribeToSaveResponse(result: Observable<HttpResponse<Contact>>): void {
    result.subscribe(
      () => this.onSaveSuccess(),
      () => this.onSaveError(),
      () => this.eventManager.broadcast('modifContact')
    );
  }

  protected onSaveSuccess(): void {
    // this.showModal = false;
    const entityNameUpdate = this.translateService.instant('contact.message.create');

    this.toastr.success(entityNameUpdate);

    this.submitted = false;
    this.registerForm.reset();
    this.registerForm.markAsPristine();
    if (this.enregister) {
      this.activeModal.close();
      this.enregister = false;
      this.ContainerClass = 'container';
    }
    // this.previousState();
    this.eventManager.broadcast('modifContact');
  }

  protected onSaveError(): void {
    // this.showModal = false;
    this.emailerror = !this.emailerror;
    this.emailerrorMsg = 'Email existe deja';
    this.toastr.error('Contact existe deja');
  }

  /* check all */
  checkAll(ev: any): void {
    this.contacts.forEach(x => (x.state = ev.target.checked));
  }

  isAllChecked(): boolean {
    return this.contacts.every(_ => _.state);
  }

  onClickMe(): void {
    this.enregister = true;
  }
  /*
    showSuccess():void {
      this.toastr.success('Hello world!', 'Toastr fun!');
    }*/

  // open pop up
  showAccount(): void {
    this.ajoutcompteModalservice.open();
  }
}
