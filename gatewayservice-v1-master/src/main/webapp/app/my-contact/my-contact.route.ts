import { Routes } from '@angular/router';
import { contactListRoute } from 'app/my-contact/contact-list/contact-list.route';
import { contactDetailsRoute } from './contact-details/contact-details.route';

const MY_CONTACT_ROUTES = [contactListRoute, contactDetailsRoute];

export const MyContactState: Routes = [
  {
    path: '',
    children: MY_CONTACT_ROUTES
  }
];
