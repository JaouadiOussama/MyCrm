import { Route } from '@angular/router';
import { ContactListComponent } from 'app/my-contact/contact-list/contact-list.component';

export const contactListRoute: Route = {
  path: 'contactlist',
  component: ContactListComponent
  /* data: {
    authorities: [],
    pageTitle: 'contactlist.title'
  }*/
};
