import { Component, OnInit, OnDestroy } from '@angular/core';
import { Contact } from 'app/shared/services/model';
import { JhiEventManager } from 'ng-jhipster';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { ContactService } from 'app/shared/services/my-contact.service';
import { HttpResponse, HttpHeaders } from '@angular/common/http';
import { Subscription } from 'rxjs';
import { AjoutcontactModalService } from 'app/shared/services/ajoutconact-modal.service';
import { ContactDeleteComponent } from '../contact-delete/contact-delete.component';

@Component({
  selector: 'jhi-contact-list',
  templateUrl: './contact-list.component.html',
  styleUrls: ['./contact-list.component.scss']
})
export class ContactListComponent implements OnInit, OnDestroy {
  contacts: Contact[] = [];
  enventSubscriber?: Subscription;
  checkedClicked = false;
  allButtonChecked = false;
  NamesAscending = true;

  /* deleted */
  toBeDeleted: number[] = [];
  toBeDeletedApi: number[] = [];

  /* pagination */
  page = 1;
  pageSize = 10;
  totalItems = 0;

  /* recherche */
  beans: Contact[] = [];
  beansFilter = '';
  beansAscending = true;

  constructor(
    protected contactService: ContactService,
    protected eventManager: JhiEventManager,
    protected modalService: NgbModal,
    protected ajoutcontactModalservice: AjoutcontactModalService
  ) {}

  loadAll(): void {
    this.contactService.findByDeleted(0).subscribe((res: HttpResponse<Contact[]>) => {
      this.onSuccess(res.body, res.headers);
      this.filterAndSortBeans();
    });
  }
  private onSuccess(contacts: Contact[] | null, headers: HttpHeaders): void {
    this.totalItems = Number(headers.get('X-Total-Count'));
    this.contacts = contacts || [];
  }

  ngOnInit(): void {
    this.loadAll();
    this.registerChangeContact();
    if (!this.toBeDeletedApi.length) {
      this.toBeDeleted = [];
    }
  }

  ngOnDestroy(): void {
    if (this.enventSubscriber) {
      this.eventManager.destroy(this.enventSubscriber);
    }
  }
  reload(): void {
    this.loadAll();
    // this.companies = this.companies?.filter((currentValue, index) => !this.toBeDeleted.includes(index));
    this.contacts = this.contacts?.filter((currentValue, index) => !this.toBeDeleted.includes(index));
    this.toBeDeleted = [];
    this.checkedClicked = false;
    this.allButtonChecked = false;
  }

  registerChangeContact(): void {
    this.enventSubscriber = this.eventManager.subscribe('modifContact', () => this.reload());
  }

  trackId(index: number, item: Contact): number {
    // eslint-disable-next-line @typescript-eslint/no-unnecessary-type-assertion
    return item.id!;
  }

  checkAll(event: any): void {
    if (event.target.checked) {
      this.checkedClicked = true;
      this.allButtonChecked = true;
      for (let i = 0; i < this.contacts?.length; i++) {
        this.toBeDeleted.push(i);
        const id = this.contacts[i].id;
        this.toBeDeletedApi.push(id!);
      }
    } else {
      this.checkedClicked = false;
      this.allButtonChecked = false;
      this.toBeDeleted = [];
      this.toBeDeletedApi = [];
    }
  }

  checkOne(): void {
    //  this.checkedClicked = !this.checkedClicked;
    if (!this.toBeDeleted.length && !this.allButtonChecked) {
      this.checkedClicked = false;
    } else {
      this.checkedClicked = true;
    }
  }

  delete(): void {
    const modalRef = this.modalService.open(ContactDeleteComponent, { size: 'md', backdrop: 'static' });
    modalRef.componentInstance.toBeDeletedApi = this.toBeDeletedApi;
    this.toBeDeletedApi = [];
  }
  remove(event: any, index: any, indexApi: number): void {
    if (event.target.checked) {
      this.toBeDeleted.push(index);
      this.toBeDeletedApi.push(indexApi);
    } else {
      const idx = this.toBeDeleted.indexOf(index);
      this.toBeDeleted.splice(idx, 1);
      const idxApi = this.toBeDeletedApi.indexOf(indexApi);
      this.toBeDeletedApi.splice(idxApi, 1);
    }
  }

  show(): void {
    this.ajoutcontactModalservice.open();
  }
  SortName(): void {
    this.contacts = this.contacts.sort((a, b) =>
      (a.persLastName ? a.persLastName : '') < (b.persLastName ? b.persLastName : '')
        ? this.NamesAscending
          ? -1
          : 1
        : this.NamesAscending
        ? 1
        : -1
    );
  }

  filterAndSortBeans(): void {
    this.beans = this.contacts
      .filter(bean => !this.beansFilter || bean.infoName?.toLowerCase().includes(this.beansFilter.toLowerCase()))
      .sort((a, b) =>
        (a.infoName ? a.infoName : '') < (b.infoName ? b.infoName : '') ? (this.beansAscending ? -1 : 1) : this.beansAscending ? 1 : -1
      ); // une erreur qui apparait lors de la compilation mais c'est fonctionnel !!!
  }
}
