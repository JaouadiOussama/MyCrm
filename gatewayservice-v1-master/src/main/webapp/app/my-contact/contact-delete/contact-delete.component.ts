import { Component } from '@angular/core';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { Contact, CContact } from 'app/shared/services/model';

import { HttpResponse } from '@angular/common/http';
import { ToastrService } from 'ngx-toastr';
import { JhiEventManager } from 'ng-jhipster';
import { ContactService } from 'app/shared/services/my-contact.service';
import { TranslateService } from '@ngx-translate/core';

@Component({
  selector: 'jhi-contact-delete',
  templateUrl: './contact-delete.component.html'
})
export class ContactDeleteComponent {
  contact!: Contact;
  toBeDeletedApi: number[] = [];

  // toBeDeleted: number[] = [];
  deleted = false;

  constructor(
    protected contactService: ContactService,
    public activeModal: NgbActiveModal,
    protected toastr: ToastrService,
    protected eventManager: JhiEventManager,
    protected translateService: TranslateService
  ) {}

  cancel(): void {
    this.eventManager.broadcast('modifContact');
    this.activeModal.dismiss();
    //  this.toBeDeleted=[];
  }
  confirmDelete(): void {
    for (let i = 0; i < this.toBeDeletedApi.length; i++) {
      this.contactService.find(this.toBeDeletedApi[i]).subscribe((res: HttpResponse<Contact>) => {
        this.contact = res.body || new CContact();
        this.contact.deleted = 1;
        this.contactService.update(this.contact).subscribe(() => {
          this.eventManager.broadcast('modifContact');
        });
      });
    }
    const entityNameUpdate = this.translateService.instant('contact.message.deleted');
    if (this.toBeDeletedApi.length === 1) {
      this.toastr.success(entityNameUpdate);
    } else {
      this.toastr.success(this.toBeDeletedApi.length + entityNameUpdate);
    }
    this.deleted = true;
    this.activeModal.close(this.deleted);
  }
}
