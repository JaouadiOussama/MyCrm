import { NgModule } from '@angular/core';
import { GatewayServiceSharedModule } from 'app/shared/shared.module';
import { NgSelectModule } from '@ng-select/ng-select';
import { RouterModule } from '@angular/router';
import { OpportunityListComponent } from './opportunity-list/opportunity-list.component';
import { MyOpportunityState } from './my-opportunities.route';
import { AjoutOpportunityModalComponent } from './opportunity-ajout/opportunity-ajout.component';
import { OpportunityDeleteComponent } from './opportunity-delete/opportunity-delete.component';
import { OpportunityDetailsComponent } from './opportunity-details/opportunity-details.component';

@NgModule({
  imports: [GatewayServiceSharedModule, NgSelectModule, RouterModule.forChild(MyOpportunityState)],
  declarations: [OpportunityListComponent, AjoutOpportunityModalComponent, OpportunityDeleteComponent, OpportunityDetailsComponent],
  entryComponents: [OpportunityDeleteComponent]
})
export class MyOpportunitiesModule {}
