import { Routes } from '@angular/router';
import { opportunityListRoute } from './opportunity-list/opportunity-list.route';
import { opportunitydetailsRoute } from './opportunity-details/opportunity-details.route';

const MY_OPPORTUNITY_ROUTES = [opportunityListRoute, opportunitydetailsRoute];

export const MyOpportunityState: Routes = [
  {
    path: '',
    children: MY_OPPORTUNITY_ROUTES
  }
];
