import { Route } from '@angular/router';
import { OpportunityListComponent } from './opportunity-list.component';

export const opportunityListRoute: Route = {
  path: 'opportunitylist',
  component: OpportunityListComponent
  /* data: {
      authorities: [],
      pageTitle: 'contactlist.title'
    }*/
};
