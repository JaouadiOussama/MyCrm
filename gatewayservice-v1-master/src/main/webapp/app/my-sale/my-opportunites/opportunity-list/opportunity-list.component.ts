import { Component, OnInit, OnDestroy } from '@angular/core';
import { Opportunity } from 'app/shared/services/opportunityService/module';
import { OpportunityService } from 'app/shared/services/opportunityService/opportunity.service';
import { JhiEventManager } from 'ng-jhipster';
import { HttpResponse, HttpHeaders } from '@angular/common/http';
import { Subscription } from 'rxjs';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { OpportunityDeleteComponent } from '../opportunity-delete/opportunity-delete.component';
import { AjoutOpportunityModalComponent } from '../opportunity-ajout/opportunity-ajout.component';

@Component({
  selector: 'jhi-opportunity-list',
  templateUrl: './opportunity-list.component.html',
  styleUrls: ['./opportunity-list.component.scss']
})
export class OpportunityListComponent implements OnInit, OnDestroy {
  opportunities!: Opportunity[];
  beans: Opportunity[] = [];
  beansFilter = '';
  beansAscending = true;

  checkedClicked = false;
  allButtonChecked = false;

  /* deleted */
  toBeDeleted: number[] = [];
  toBeDeletedApi: number[] = [];

  /* pagination */
  page = 1;
  pageSize = 10;
  totalItems = 0;

  enventSubscriber?: Subscription;

  constructor(
    protected opportunityService: OpportunityService,
    protected eventManager: JhiEventManager,
    protected modalService: NgbModal
  ) {}

  ngOnInit(): void {
    this.loadAll();
    if (!this.toBeDeletedApi.length) {
      this.toBeDeleted = [];
    }
    this.registerChangeOpporunity();
    this.cancelDelete();
  }

  ngOnDestroy(): void {
    if (this.enventSubscriber) {
      this.eventManager.destroy(this.enventSubscriber);
    }
  }

  loadAll(): void {
    this.opportunityService.findByDeleted(0).subscribe((res: HttpResponse<Opportunity[]>) => {
      this.onSuccess(res.body, res.headers);
      this.filterAndSortBeans();
    });
  }

  private onSuccess(beans: Opportunity[] | null, headers: HttpHeaders): void {
    this.totalItems = Number(headers.get('X-Total-Count'));
    this.opportunities = beans || [];
  }
  filterAndSortBeans(): void {
    this.beans = this.opportunities
      .filter(bean => !this.beansFilter || bean.oppName?.toLowerCase().includes(this.beansFilter.toLowerCase()))
      .sort((a, b) =>
        (a.oppName ? a.oppName : '') < (b.oppName ? b.oppName : '') ? (this.beansAscending ? -1 : 1) : this.beansAscending ? 1 : -1
      ); // une erreur qui apparait lors de la compilation mais c'est fonctionnel !!!
  }

  checkAll(event: any): void {
    if (event.target.checked) {
      this.checkedClicked = true;
      this.allButtonChecked = true;
      for (let i = 0; i < this.beans?.length; i++) {
        this.toBeDeleted.push(i);
      }
    } else {
      this.checkedClicked = false;
      this.allButtonChecked = false;
      this.toBeDeleted = [];
    }
  }

  checkOne(): void {
    // this.checkedClicked = !this.checkedClicked;
    if (!this.toBeDeleted.length && !this.allButtonChecked) {
      this.checkedClicked = false;
    } else {
      this.checkedClicked = true;
    }
  }

  trackId(index: number, item: Opportunity): number {
    // eslint-disable-next-line @typescript-eslint/no-unnecessary-type-assertion
    return item.id!;
  }
  remove(event: any, index: any, indexApi: number): void {
    if (event.target.checked) {
      this.toBeDeleted.push(index);
      this.toBeDeletedApi.push(indexApi);
    } else {
      const idx = this.toBeDeleted.indexOf(index);
      this.toBeDeleted.splice(idx, 1);
      const idxApi = this.toBeDeletedApi.indexOf(indexApi);
      this.toBeDeletedApi.splice(idxApi, 1);
    }
  }
  // ajout
  show(): void {
    const modalRef = this.modalService.open(AjoutOpportunityModalComponent, { size: 'md', backdrop: 'static' });

    modalRef.componentInstance.create = true;
  }
  // delete

  delete(): void {
    const modalRef = this.modalService.open(OpportunityDeleteComponent, { size: 'md', backdrop: 'static' });
    modalRef.componentInstance.toBeDeletedApi = this.toBeDeletedApi;
    this.toBeDeletedApi = [];
  }

  reload(): void {
    this.loadAll();
    this.beans = this.beans?.filter((currentValue, index) => !this.toBeDeleted.includes(index));
    this.toBeDeleted = [];
    this.checkedClicked = false;
    this.allButtonChecked = false;
  }
  registerChangeOpporunity(): void {
    this.enventSubscriber = this.eventManager.subscribe('modifOpportunity', () => this.reload());
  }
  cancelDelete(): void {
    this.enventSubscriber = this.eventManager.subscribe('cancelDelete', () => this.reload());
  }
}
