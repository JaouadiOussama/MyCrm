import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

import { HttpResponse } from '@angular/common/http';

import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { JhiEventManager } from 'ng-jhipster';
import { Subscription } from 'rxjs';

import { Phone, PersonalInfo, Profil } from 'app/shared/services/model';

import { OpportunityService } from 'app/shared/services/opportunityService/opportunity.service';
import { Opportunity } from 'app/shared/services/opportunityService/module';
import { AjoutOpportunityModalComponent } from '../opportunity-ajout/opportunity-ajout.component';

@Component({
  selector: 'jhi-opportunity-details',
  templateUrl: './opportunity-details.component.html',
  styleUrls: ['./opportunity-details.component.scss']
})
export class OpportunityDetailsComponent implements OnInit {
  id: any;
  opportunity: any;
  resume = false;
  coordonne = true;
  personalInfos: PersonalInfo[] = [];
  personalInfoId = 0;
  companyId = 0;
  phones: Phone[] = [];
  listPhones: Phone[] = [];
  profils?: Profil[];
  listefaceBook?: Profil[];
  facebookUrl: any;
  listeTwitter?: Profil[];
  twitterUrl: any;
  listeLindeIn?: Profil[];
  linkedInUrl: any;
  buttonColorResume = '#cecece';
  buttonColorCoordonne = '';

  enventSubscriber?: Subscription;

  constructor(
    private route: ActivatedRoute,
    protected opportunityService: OpportunityService,
    protected modalService: NgbModal,
    protected eventManager: JhiEventManager
  ) {}

  ngOnInit(): void {
    this.id = this.route.snapshot.params['id'];

    this.getById();
    this.registerChangeOpporunity();
  }

  getById(): void {
    this.opportunityService.find(this.id).subscribe((res: HttpResponse<Opportunity>) => (this.opportunity = res.body || []));
  }

  displayResume(): void {
    this.resume = false;
    this.coordonne = true;
    this.buttonColorCoordonne = ''; // desired Color
    this.buttonColorResume = '#cecece';
  }

  displayCoordonne(): void {
    this.coordonne = false;
    this.resume = true;
    this.buttonColorCoordonne = '#cecece'; // desired Color
    this.buttonColorResume = '';
  }
  /**
   * update opportunty
   */
  update(opportunity?: Opportunity): void {
    const modalRef = this.modalService.open(AjoutOpportunityModalComponent, { size: 'md', backdrop: 'static' });
    modalRef.componentInstance.opportunity = opportunity;
    // modalRef.componentInstance.infoId = this.infoId;
    //   modalRef.componentInstance.codeopportunity = this.country?.codeopportunity;
    if (opportunity) {
      modalRef.componentInstance.create = false;
    } else {
      modalRef.componentInstance.create = true;
    }
  }

  registerChangeOpporunity(): void {
    this.enventSubscriber = this.eventManager.subscribe('modifOpportunity', () => this.getById());
  }
}
