import { Route } from '@angular/router';
import { OpportunityDetailsComponent } from './opportunity-details.component';

export const opportunitydetailsRoute: Route = {
  path: 'opportunitydetails/:id',
  component: OpportunityDetailsComponent
  /* data: {
    authorities: [],
    pageTitle: 'accountlist.title'
  }*/
};
