import { Component, OnInit, OnDestroy } from '@angular/core';
import { AjoutOpportunityModalService } from 'app/shared/services/opportunityService/ajoutopportunity-modal.service';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { FormBuilder, Validators } from '@angular/forms';
import {
  Opportunity,
  CompanyOpportunity,
  ContactOpportunity,
  COpportunity,
  Stage,
  Currency
} from 'app/shared/services/opportunityService/module';
import { HttpResponse, HttpErrorResponse } from '@angular/common/http';
import { Observable, Subscription } from 'rxjs';
import { ToastrService } from 'ngx-toastr';
import { JhiEventManager, JhiParseLinks, JhiEventWithContent } from 'ng-jhipster';
import { OpportunityService } from 'app/shared/services/opportunityService/opportunity.service';
import { CompanyOpportunityService } from 'app/shared/services/opportunityService/company.service';
import { TranslateService } from '@ngx-translate/core';
import { ContactOpportunityService } from 'app/shared/services/opportunityService/contact.service';
import { StageService } from 'app/shared/services/opportunityService/stage.service';
import { CurrencyService } from 'app/shared/services/opportunityService/currencie.service';
import { DatePipe } from '@angular/common';

@Component({
  selector: 'jhi-ajout-opportunity-modal',
  templateUrl: './opportunity-ajout.component.html',
  styleUrls: ['./opportunity-ajout.component.scss']
})
export class AjoutOpportunityModalComponent implements OnInit, OnDestroy {
  showModal = false;

  isSaving = false;
  enregister = false;
  create = false;
  ContainerClass = 'container';

  companies?: CompanyOpportunity[];
  contacts?: ContactOpportunity[];
  currencies?: Currency[];
  stages: any;
  dateNow = new Date();

  types = [{ name: 'Client existant' }, { name: 'Client non existant' }];

  selectedStage: any;
  selectedCompte: any;
  byFirst!: string;
  byResult!: number;

  probabilte!: string;

  registerForm = this.fb.group({
    id: [],
    nom: ['', [Validators.required]],
    montant: ['', Validators.required],
    compte: ['', Validators.required],
    dateCloture: [],
    contact: [],
    prospect: [],
    typeOpportunite: [],
    origineProspect: [],
    etape: ['', Validators.required],
    chiffreAffaire: [],
    probabilite: [],
    currency: [],
    raisonDePerte: []
  });
  httpErrorListener!: Subscription;
  selectedContact!: any;
  // update

  opportunity?: Opportunity;

  iSdisabled = true;
  opportunityName?: String;

  constructor(
    protected opportunityService: OpportunityService,
    protected ajoutOpportunityModalService: AjoutOpportunityModalService,
    protected companyService: CompanyOpportunityService,
    protected contactOpportunityService: ContactOpportunityService,
    protected currencyService: CurrencyService,
    protected stageService: StageService,
    protected fb: FormBuilder,
    public activeModal: NgbActiveModal,
    protected toastr: ToastrService,
    protected parseLinks: JhiParseLinks,
    protected eventManager: JhiEventManager,
    public translateService: TranslateService,
    private datePipe: DatePipe
  ) {
    this.httpErrorListener = this.eventManager.subscribe(
      'gatewayServiceApp.httpError',
      (response: JhiEventWithContent<HttpErrorResponse>) => {
        const httpErrorResponse = response.content;
        const entityNameUniqueUrl = this.translateService.instant('opportunity.message.uniqueOppName');

        switch (httpErrorResponse.status) {
          case 400:
            this.toastr.error(entityNameUniqueUrl);

            break;
        }
      }
    );
  }
  ngOnInit(): void {
    this.loadAll();

    this.dateNow ? this.datePipe.transform(this.dateNow, 'yyyy-MM-dd') : this.datePipe.transform(this.dateNow, 'yyyy-MM-dd');
    if (this.opportunity !== undefined) {
      this.updateForm(this.opportunity);
      this.opportunityName = this.opportunity?.oppName;
    }
  }
  loadAll(): void {
    this.companyService.query().subscribe((res: HttpResponse<CompanyOpportunity[]>) => (this.companies = res.body || []));

    this.contactOpportunityService.query().subscribe((res: HttpResponse<ContactOpportunity[]>) => (this.contacts = res.body || []));

    this.stageService.query().subscribe((res: HttpResponse<Stage[]>) => (this.stages = res.body || []));

    this.currencyService.query().subscribe((res: HttpResponse<Currency[]>) => (this.currencies = res.body || []));
  }

  show(): void {
    this.ajoutOpportunityModalService.open();
  }

  // Bootstrap Modal Close event
  hide(): void {
    this.showModal = false;
    this.ContainerClass = 'container';
  }

  // convenience getter for easy access to form fields
  // eslint-disable-next-line @typescript-eslint/tslint/config
  get f() {
    return this.registerForm.controls;
  }

  onSubmit(): void {
    this.isSaving = true;
    const opportunity = this.createFormForm();
    if (this.registerForm.invalid) {
      return;
    }

    if (this.create === false) {
      this.subscribeToSaveResponse(this.opportunityService.update(opportunity));
    } else {
      this.subscribeToSaveResponse(this.opportunityService.create(opportunity));
    }
  }

  // creation opportunite

  private createFormForm(): Opportunity {
    const oppo = new COpportunity();
    (oppo.id = this.registerForm.get(['id'])!.value), (oppo.oppName = this.registerForm.get(['nom'])!.value);
    oppo.oppValue = this.registerForm.get(['montant'])!.value;
    oppo.projEndate = this.registerForm.get(['dateCloture'])!.value;

    oppo.oppType = this.registerForm.get(['typeOpportunite'])!.value;
    oppo.oppLeadOrigin = this.registerForm.get(['origineProspect'])!.value;
    oppo.deleted = 0;
    if (this.registerForm.get(['contact'])!.value) {
      oppo.contact = this.createContact();
    }
    if (this.registerForm.get(['compte'])!.value) {
      oppo.company = this.createCompany();
    }
    if (this.registerForm.get(['currency'])!.value) {
      oppo.currency = this.createCurrency();
    }

    if (this.registerForm.get(['etape'])!.value) {
      oppo.stage = this.createStage();
    }
    oppo.oppTurnover = this.byResult;
    oppo.projOpp = 0;
    oppo.createdAt = this.dateNow;
    oppo.updatedAt = this.dateNow;
    // oppo.currency=1;

    // raison de perte
    oppo.oppPertCause = this.registerForm.get(['raisonDePerte'])!.value;

    return oppo;
  }
  private createCompany(): CompanyOpportunity {
    return {
      id: this.registerForm.get(['compte'])!.value
    };
  }

  private createContact(): ContactOpportunity {
    return {
      id: this.registerForm.get(['contact'])!.value
    };
  }

  private createStage(): Stage {
    return {
      id: this.selectedStage?.id
    };
  }
  private createCurrency(): Currency {
    return {
      id: this.registerForm.get(['currency'])!.value
    };
  }

  protected subscribeToSaveResponse(result: Observable<HttpResponse<Opportunity>>): void {
    result.subscribe(
      () => this.onSaveSuccess(),
      () => this.onSaveError()
    );
  }

  protected onSaveSuccess(): void {
    // this.showModal = false;
    const entityNameCreated = this.translateService.instant('opportunity.message.create');
    const entityNameUpdate = this.translateService.instant('opportunity.message.update');
    this.isSaving = false;
    if (this.create) {
      this.toastr.success(entityNameCreated);
      // this.toastr.success("opportunité crée avec succes");
    } else {
      this.toastr.success(entityNameUpdate);
      // this.toastr.success("Mise à jours opportunité  avec succes");
    }

    // this.previousState();
    this.eventManager.broadcast('modifOpportunity');
    this.registerForm.reset();
    this.registerForm.markAsPristine();
    if (this.enregister) {
      this.activeModal.close();
      this.enregister = false;
      this.ContainerClass = 'container';
    }
  }

  protected onSaveError(): void {
    // this.showModal = false;
    this.isSaving = false;
    this.toastr.error('Erreur d ajout opportunité ');
  }

  ngOnDestroy(): void {
    if (this.httpErrorListener) {
      this.eventManager.destroy(this.httpErrorListener);
    }
  }

  multiplication(): void {
    this.byResult = (parseFloat(this.byFirst) * parseFloat(this.probabilte)) / 100;
    if (isNaN(this.byResult)) {
      this.byResult = parseFloat(this.byFirst) ? parseFloat(this.byFirst) : 0;
    }
  }
  addprobability(): void {
    this.probabilte = this.selectedStage?.stageProbability;
    this.multiplication();
  }

  /**
   * update
   */
  updateForm(opportunity: Opportunity): void {
    this.registerForm.patchValue({
      id: opportunity.id,
      nom: opportunity.oppName,
      montant: opportunity.oppValue,
      compte: opportunity.company?.id,
      dateCloture: opportunity.projEndate,
      contact: opportunity.contact?.id,

      typeOpportunite: opportunity.oppType,
      origineProspect: opportunity.oppLeadOrigin,
      etape: opportunity.stage?.id,
      chiffreAffaire: opportunity.oppTurnover,
      probabilite: opportunity.stage?.stageProbability,
      currency: opportunity.currency?.id,
      raisonDePerte: opportunity.oppPertCause
    });
    this.byFirst = opportunity.oppValue?.toString() || '';
    this.selectedStage = opportunity.stage;
    this.addprobability();
  }

  cancel(): void {
    this.activeModal.dismiss();
  }

  onClickMe(): void {
    this.enregister = true;
  }
}
