import { Component } from '@angular/core';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { Opportunity, COpportunity } from 'app/shared/services/opportunityService/module';

import { HttpResponse } from '@angular/common/http';
import { ToastrService } from 'ngx-toastr';
import { JhiEventManager } from 'ng-jhipster';
import { OpportunityService } from 'app/shared/services/opportunityService/opportunity.service';
import { TranslateService } from '@ngx-translate/core';

@Component({
  selector: 'jhi-opportunity-delete',
  templateUrl: './opportunity-delete.component.html'
})
export class OpportunityDeleteComponent {
  opportunity!: Opportunity;
  toBeDeletedApi: number[] = [];

  // toBeDeleted: number[] = [];
  deleted = false;

  constructor(
    protected opportunityService: OpportunityService,
    public activeModal: NgbActiveModal,
    protected toastr: ToastrService,
    protected eventManager: JhiEventManager,
    public translateService: TranslateService
  ) {}

  cancel(): void {
    this.eventManager.broadcast('cancelDelete');
    this.activeModal.dismiss();
  }
  confirmDelete(): void {
    for (let i = 0; i < this.toBeDeletedApi.length; i++) {
      this.opportunityService.find(this.toBeDeletedApi[i]).subscribe((res: HttpResponse<Opportunity>) => {
        this.opportunity = res.body || new COpportunity();
        this.opportunity.deleted = 1;
        this.opportunityService.update(this.opportunity).subscribe(() => {
          this.eventManager.broadcast('modifOpportunity');
        });
      });
    }
    const entityNamesupprime = this.translateService.instant('opportunity.message.supprime');
    const entityNamesupprimes = this.translateService.instant('opportunity.message.supprimes');

    if (this.toBeDeletedApi.length === 1) {
      this.toastr.success(entityNamesupprime);
    } else {
      this.toastr.success(this.toBeDeletedApi.length + entityNamesupprimes);
    }
    this.deleted = true;
    this.activeModal.close(this.deleted);
  }
}
