import { Component, OnInit, OnDestroy } from '@angular/core';
import { ProfilService } from 'app/shared/services/profil.service';
import { ActivatedRoute } from '@angular/router';
import { FormBuilder, Validators } from '@angular/forms';
import { Profil, CProfil, Info } from 'app/shared/services/model';
import { Observable, Subscription } from 'rxjs';
import { HttpResponse, HttpErrorResponse } from '@angular/common/http';
import { JhiEventManager, JhiEventWithContent } from 'ng-jhipster';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { ToastrService } from 'ngx-toastr';
import { TranslateService } from '@ngx-translate/core';

@Component({
  selector: 'jhi-profil-update',
  templateUrl: './profil-update.component.html',
  styleUrls: ['./profil-update.component.scss']
})
export class ProfilUpdateComponent implements OnInit, OnDestroy {
  isSaving = false;
  profil?: Profil;
  create = false;
  infoId = 0;
  codeProfil = 0;

  httpErrorListener!: Subscription;

  profilType = [
    { name: 'LinkedIn', icon: 'fab fa-linkedin' },
    { name: 'Facebook', icon: 'fab fa-facebook' },
    { name: 'Twitter', icon: 'fab fa-twitter-square' }
  ];

  editForm = this.fb.group({
    id: [],
    profilName: [null, Validators.required],
    profilUrl: [null, Validators.required]
  });

  constructor(
    protected profilService: ProfilService,
    protected activatedRoute: ActivatedRoute,
    private fb: FormBuilder,
    protected toastr: ToastrService,
    protected eventManager: JhiEventManager,
    public activeModal: NgbActiveModal,
    public translateService: TranslateService
  ) {
    this.httpErrorListener = this.eventManager.subscribe(
      'gatewayServiceApp.httpError',
      (response: JhiEventWithContent<HttpErrorResponse>) => {
        const httpErrorResponse = response.content;
        const entityNameUniqueUrl = this.translateService.instant('profil.message.uniqueUrl');

        switch (httpErrorResponse.status) {
          case 400:
            this.toastr.error(entityNameUniqueUrl);

            break;
        }
      }
    );
  }

  cancel(): void {
    this.activeModal.dismiss();
  }
  ngOnInit(): void {
    if (this.profil !== undefined) {
      this.updateForm(this.profil);
    }
  }

  updateForm(profil: Profil): void {
    this.editForm.patchValue({
      id: profil.id,
      profilName: profil.profil,

      profilUrl: profil.url
    });
  }

  previousState(): void {
    window.history.back();
  }

  save(): void {
    this.isSaving = true;
    const profil = this.createFromForm();
    if (this.create === false) {
      this.subscribeToSaveResponse(this.profilService.update(profil));
    } else {
      this.subscribeToSaveResponse(this.profilService.create(profil));
    }
  }
  private createFromForm(): Profil {
    return {
      ...new CProfil(),
      id: this.editForm.get(['id'])!.value,
      profil: this.editForm.get(['profilName'])!.value,

      url: this.editForm.get(['profilUrl'])!.value,
      deleted: 0,
      info: this.createInfo()
    };
  }
  private createInfo(): Info {
    return {
      id: this.infoId
    };
  }

  protected subscribeToSaveResponse(result: Observable<HttpResponse<Profil>>): void {
    result.subscribe(
      () => this.onSaveSuccess(),
      () => this.onSaveError()
    );
  }

  protected onSaveSuccess(): void {
    this.isSaving = false;
    const entityNameCreated = this.translateService.instant('profil.message.create');
    const entityNameUpdate = this.translateService.instant('profil.message.update');

    if (this.create) {
      this.toastr.success(entityNameCreated);
    } else {
      this.toastr.success(entityNameUpdate);
    }
    this.eventManager.broadcast('ProfilModification');
    this.activeModal.close();
  }

  protected onSaveError(): void {
    this.isSaving = false;
  }

  ngOnDestroy(): void {
    if (this.httpErrorListener) {
      this.eventManager.destroy(this.httpErrorListener);
    }
  }
}
