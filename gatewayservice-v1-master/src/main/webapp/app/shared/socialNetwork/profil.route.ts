import { Resolve, Router, ActivatedRouteSnapshot, Routes } from '@angular/router';
import { Profil, CProfil } from 'app/shared/services/model';

import { Injectable } from '@angular/core';
import { Observable, of, EMPTY } from 'rxjs';
import { flatMap } from 'rxjs/operators';
import { HttpResponse } from '@angular/common/http';

import { Authority } from 'app/shared/constants/authority.constants';
import { UserRouteAccessService } from 'app/core/auth/user-route-access-service';
import { ProfilUpdateComponent } from './profil-update.component';
import { ProfilService } from '../services/profil.service';

@Injectable({ providedIn: 'root' })
export class ProfilResolve implements Resolve<Profil> {
  constructor(private service: ProfilService, private router: Router) {}

  resolve(route: ActivatedRouteSnapshot): Observable<Profil> | Observable<never> {
    const id = route.params['id'];
    if (id) {
      return this.service.find(id).pipe(
        flatMap((profil: HttpResponse<Profil>) => {
          if (profil.body) {
            return of(profil.body);
          } else {
            this.router.navigate(['404']);
            return EMPTY;
          }
        })
      );
    }
    return of(new CProfil());
  }
}

export const profilRoute: Routes = [
  {
    path: 'profil/new',
    component: ProfilUpdateComponent,
    resolve: {
      profil: ProfilResolve
    },
    data: {
      authorities: [Authority.USER],
      pageTitle: 'profils'
    },
    canActivate: [UserRouteAccessService]
  },

  {
    path: 'profil/:id/edit',
    component: ProfilUpdateComponent,
    resolve: {
      profil: ProfilResolve
    },
    data: {
      authorities: [Authority.USER],
      pageTitle: 'profils'
    },
    canActivate: [UserRouteAccessService]
  }
];
