import { Component, OnInit, OnDestroy, Input } from '@angular/core';
import { Profil, Country } from 'app/shared/services/model';
import { Subscription } from 'rxjs';

import { JhiEventManager } from 'ng-jhipster';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { HttpResponse } from '@angular/common/http';
import { ProfilService } from '../services/profil.service';
import { PersonalInfoService } from '../services/personalInfo.service';
import { ProfilUpdateComponent } from './profil-update.component';
import { ProfilDeleteDialogComponent } from './profil-delete-dailog.component';

@Component({
  selector: 'jhi-profil',
  templateUrl: './profil.component.html',
  styleUrls: ['./profil.component.scss']
})
export class ProfilComponent implements OnInit, OnDestroy {
  profils?: Profil[];
  eventSubscriber?: Subscription;
  country?: Country;

  @Input() infoId!: number;

  constructor(
    protected profilService: ProfilService,
    protected eventManager: JhiEventManager,
    protected modalService: NgbModal,
    protected personalInfoService: PersonalInfoService
  ) {}

  loadAll(): void {
    this.profilService
      .findByDeletedAndPersonalInfo(0, this.infoId)
      .subscribe((res: HttpResponse<Profil[]>) => (this.profils = res.body || []));
  }

  ngOnInit(): void {
    this.loadAll();
    this.registerChangeInProfils();
    this.personalInfoService.getCountry(this.infoId).subscribe((res: HttpResponse<Country>) => (this.country = res.body || {}));
  }

  ngOnDestroy(): void {
    if (this.eventSubscriber) {
      this.eventManager.destroy(this.eventSubscriber);
    }
  }

  trackId(index: number, item: Profil): number {
    // eslint-disable-next-line @typescript-eslint/no-unnecessary-type-assertion
    return item.id!;
  }

  registerChangeInProfils(): void {
    this.eventSubscriber = this.eventManager.subscribe('ProfilModification', () => this.loadAll());
  }

  delete(profil: Profil): void {
    const modalRef = this.modalService.open(ProfilDeleteDialogComponent, { size: 'md', backdrop: 'static' });
    modalRef.componentInstance.profil = profil;
  }
  update(profil?: Profil): void {
    const modalRef = this.modalService.open(ProfilUpdateComponent, { size: 'md', backdrop: 'static' });
    modalRef.componentInstance.profil = profil;
    modalRef.componentInstance.infoId = this.infoId;
    //   modalRef.componentInstance.codeProfil = this.country?.codeProfil;
    if (profil) {
      modalRef.componentInstance.create = false;
    } else {
      modalRef.componentInstance.create = true;
    }
  }
}
