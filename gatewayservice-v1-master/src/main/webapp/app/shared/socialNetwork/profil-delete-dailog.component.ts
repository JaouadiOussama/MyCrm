import { Profil, CProfil } from 'app/shared/services/model';
import { Component } from '@angular/core';

import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { JhiEventManager } from 'ng-jhipster';
import { HttpResponse } from '@angular/common/http';
import { ToastrService } from 'ngx-toastr';
import { ProfilService } from '../services/profil.service';
import { TranslateService } from '@ngx-translate/core';

@Component({
  templateUrl: './profil-delete-dialog.component.html'
})
export class ProfilDeleteDialogComponent {
  profil?: Profil;

  constructor(
    protected profilService: ProfilService,
    public activeModal: NgbActiveModal,
    protected toastr: ToastrService,
    protected eventManager: JhiEventManager,
    public translateService: TranslateService
  ) {}

  cancel(): void {
    this.activeModal.dismiss();
  }

  confirmDelete(id: number): void {
    const entityNameSupp = this.translateService.instant('profil.message.supprimerSuccess');
    this.profilService.find(id).subscribe((res: HttpResponse<Profil>) => {
      this.profil = res.body || new CProfil();
      this.profil.deleted = 1;
      this.profilService.update(this.profil).subscribe(() => {
        this.toastr.success(entityNameSupp);
        this.eventManager.broadcast('ProfilModification');
        this.activeModal.close();
      });
    });
  }
}
