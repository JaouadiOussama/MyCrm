import { Input, Output, OnInit, Component, EventEmitter } from '@angular/core';
import { Page } from './page';

@Component({
  selector: 'jhi-custom-pagination',
  templateUrl: './custom-pagination.component.html'
})
export class CustomPaginationComponent implements OnInit {
  @Input() page!: Page<any>;
  @Output() nextPageEvent = new EventEmitter();
  @Output() previousPageEvent = new EventEmitter();
  @Output() pageSizeEvent: EventEmitter<number> = new EventEmitter<number>();

  constructor() {}

  ngOnInit(): void {}

  nextPage(): void {
    this.nextPageEvent.emit(null);
  }

  previousPage(): void {
    this.previousPageEvent.emit(null);
  }

  updatePageSize(pageSize: number): void {
    this.pageSizeEvent.emit(pageSize);
  }
}
