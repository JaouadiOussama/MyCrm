import { Sort } from './sort';
import { Pageable } from './pageable';

export class Page<T> {
  content: Array<T> = [];
  pageable: Pageable;
  last = false;
  totalPages!: number;
  totalElements!: number;
  first = false;
  sort: Sort = new Sort();
  numberOfElements!: number;
  size!: number;
  number!: number;

  public constructor() {
    this.pageable = new Pageable();
  }
}
