import { NgModule } from '@angular/core';
import { GatewayServiceSharedLibsModule } from './shared-libs.module';
import { FindLanguageFromKeyPipe } from './language/find-language-from-key.pipe';
import { AlertComponent } from './alert/alert.component';
import { AlertErrorComponent } from './alert/alert-error.component';
import { LoginModalComponent } from './login/login.component';
import { HasAnyAuthorityDirective } from './auth/has-any-authority.directive';
import { PhoneComponent } from './phones/phone.component';
import { PhoneDeleteDialogComponent } from './phones/phone-delete-dialog.component';
import { PhoneUpdateComponent } from './phones/phone-update.component';
import { RouterModule } from '@angular/router';
import { phoneRoute } from './phones/phone.route';
import { EmailComponent } from './email/email.component';
import { EmailDeleteComponent } from './email/email-delete/email-delete.component';
import { EmailUpdateComponent } from './email/email-update/email-update.component';

import { emailRoute } from './email/email.route';
import { AdresseComponent } from './adresse/adresse.component';
import { AdresseDeleteComponent } from './adresse/adresse-delete/adresse-delete.component';
import { AdresseUpdateComponent } from './adresse/adresse-update/adresse-update.component';

import { adresseRoute } from './adresse/adresse.route';

import { NgSelectModule } from '@ng-select/ng-select';
import { ProfilComponent } from './socialNetwork/profil.component';
import { ProfilUpdateComponent } from './socialNetwork/profil-update.component';
import { ProfilDeleteDialogComponent } from './socialNetwork/profil-delete-dailog.component';

@NgModule({
  imports: [
    GatewayServiceSharedLibsModule,
    RouterModule.forChild(adresseRoute),
    RouterModule.forChild(emailRoute),
    RouterModule.forChild(phoneRoute),
    NgSelectModule
  ],
  declarations: [
    FindLanguageFromKeyPipe,
    AlertComponent,
    AlertErrorComponent,
    LoginModalComponent,
    HasAnyAuthorityDirective,
    PhoneComponent,
    PhoneDeleteDialogComponent,
    PhoneUpdateComponent,
    AdresseComponent,
    AdresseDeleteComponent,
    AdresseUpdateComponent,
    ProfilComponent,
    ProfilUpdateComponent,
    ProfilDeleteDialogComponent,
    EmailComponent,
    EmailUpdateComponent,
    EmailDeleteComponent
  ],
  entryComponents: [
    LoginModalComponent,
    PhoneDeleteDialogComponent,
    ProfilDeleteDialogComponent,
    EmailDeleteComponent,
    AdresseDeleteComponent
  ],
  exports: [
    GatewayServiceSharedLibsModule,
    FindLanguageFromKeyPipe,
    AlertComponent,
    AlertErrorComponent,
    LoginModalComponent,
    HasAnyAuthorityDirective,
    PhoneComponent,
    PhoneDeleteDialogComponent,
    PhoneUpdateComponent,
    ProfilComponent,
    ProfilUpdateComponent,
    ProfilDeleteDialogComponent,
    EmailComponent,
    EmailUpdateComponent,
    EmailDeleteComponent,
    AdresseComponent,
    AdresseDeleteComponent,
    AdresseUpdateComponent
  ]
})
export class GatewayServiceSharedModule {}
