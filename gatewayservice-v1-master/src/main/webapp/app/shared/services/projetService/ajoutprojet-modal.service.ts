import { Injectable } from '@angular/core';
import { NgbModal, NgbModalRef } from '@ng-bootstrap/ng-bootstrap';

import { AjoutProjectModalComponent } from 'app/my-project/projet-ajouter/projet-ajouter.component';

@Injectable({ providedIn: 'root' })
export class AjoutProjetModalService {
  private isOpen = false;

  constructor(private modalService: NgbModal) {}

  open(): void {
    if (this.isOpen) {
      return;
    }
    this.isOpen = true;
    const modalRef: NgbModalRef = this.modalService.open(AjoutProjectModalComponent);
    modalRef.result.finally(() => (this.isOpen = false));
  }
}
