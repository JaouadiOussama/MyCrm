import { Injectable } from '@angular/core';
import { HttpResponse, HttpClient } from '@angular/common/http';

import { SERVER_API_URL } from 'app/app.constants';
import { Observable } from 'rxjs';
import { createRequestOption } from 'app/shared/util/request-util';
import { Company } from './model';

type EntityResponseType = HttpResponse<Company>;
type EntityArrayResponseType = HttpResponse<Company[]>;

@Injectable({
  providedIn: 'root'
})
export class CompanyService {
  public resourceUrl = SERVER_API_URL + 'services/projetservice/api/companies';

  constructor(private http: HttpClient) {}
  /* create CompanyOpportunity*/
  create(company: Company): Observable<EntityResponseType> {
    return this.http.post<Company>(this.resourceUrl, company, { observe: 'response' });
  }

  /* find by id */
  find(id: number): Observable<EntityResponseType> {
    return this.http.get<Company>(`${this.resourceUrl}/${id}`, { observe: 'response' });
  }

  /* query */
  query(req?: any): Observable<EntityArrayResponseType> {
    const options = createRequestOption(req);
    return this.http.get<Company[]>(this.resourceUrl, { params: options, observe: 'response' });
  }

  update(company: Company): Observable<EntityResponseType> {
    return this.http.put<Company>(this.resourceUrl, company, { observe: 'response' });
  }
}
