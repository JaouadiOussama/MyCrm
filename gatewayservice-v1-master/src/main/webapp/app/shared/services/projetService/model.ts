import { ContactOpportunity } from 'app/shared/services/opportunityService/module';

export interface Company {
  compDeleted?: number;
  compId?: number;
  compName?: string;
  id?: number;
  opportunities?: any[];
}
export interface Employee {
  empDeleted?: number;
  empId?: number;
  empName?: string;
  id?: number;
  empCompId?: number;
}

export interface Currency {
  createdAt?: string;
  currName?: string;
  deleted?: number;
  id?: number;
  opportunities?: any[];
  updatedAt?: string;
}

export interface PricingType {
  createdAt?: string;
  deleted?: number;
  id?: number;
  opportunities?: any[];
  pricingTypeName?: string;
  updatedAt?: string;
}

export interface Pipeline {
  createdAt?: string;
  deleted?: number;
  id?: number;
  pipeName?: string;
  stages?: any[];
  updatedAt?: string;
}

export interface Stage {
  createdAt?: string;
  deleted?: number;
  id?: number;
  opportunities?: any[];
  pipeline?: Pipeline;
  stageName?: string;
  stageProbability?: number;
  updatedAt?: string;
}

export interface Opportunity {
  employee?: Employee;
  company?: Company;
  createdAt?: Date;
  currency?: Currency;
  deleted?: number;
  id?: number;
  oppClosedate?: Date;
  oppDesc?: string;
  oppLeadOrigin?: string;
  oppName?: string;
  oppPertCause?: string;
  oppType?: string;
  pricingType?: PricingType;
  projEndate?: string;
  projOpp?: number;
  projPriority?: string;
  projStartdate?: Date;
  stage?: Stage;
  updatedAt?: Date;
}
export class COpportunity implements Opportunity {
  constructor(
    public company?: Company,
    public contactOpportunity?: ContactOpportunity,
    public createdAt?: Date,
    public currency?: Currency,
    public deleted?: number,
    public id?: number,
    public oppClosedate?: Date,
    public oppDesc?: string,
    public oppLeadOrigin?: string,
    public oppName?: string,
    public oppPertCause?: string,
    public oppTurnover?: number,
    public oppType?: string,
    public oppValue?: number,
    public stage?: Stage,
    public pricingType?: PricingType,
    public projEndate?: string,
    public projOpp?: number,
    public projPriority?: string,
    public projStartdate?: Date,
    public updatedAt?: Date
  ) {}
}
