import { Injectable } from '@angular/core';
import { HttpResponse, HttpClient } from '@angular/common/http';
import { Country, City } from './model';
import { SERVER_API_URL } from 'app/app.constants';
import { Observable } from 'rxjs';
import { createRequestOption } from 'app/shared/util/request-util';

type EntityResponseType = HttpResponse<Country>;
type EntityArrayResponseType = HttpResponse<Country[]>;

@Injectable({
  providedIn: 'root'
})
export class CountryService {
  public resourceUrl = SERVER_API_URL + 'services/personmicroservice/api/countries';

  constructor(private http: HttpClient) {}
  /* create Country*/
  create(country: Country): Observable<EntityResponseType> {
    return this.http.post<Country>(this.resourceUrl, country, { observe: 'response' });
  }

  /* find by id */
  find(id: number): Observable<EntityResponseType> {
    return this.http.get<Country>(`${this.resourceUrl}/${id}`, { observe: 'response' });
  }

  findCities(id: number): Observable<HttpResponse<City[]>> {
    return this.http.get<City[]>(`${this.resourceUrl}/${id}/cities`, { observe: 'response' });
  }

  /* query */
  query(req?: any): Observable<EntityArrayResponseType> {
    const options = createRequestOption(req);
    return this.http.get<Country[]>(this.resourceUrl, { params: options, observe: 'response' });
  }

  update(country: Country): Observable<EntityResponseType> {
    return this.http.put<Country>(this.resourceUrl, country, { observe: 'response' });
  }

  /* query */
  queryLastInsert(): Observable<EntityArrayResponseType> {
    const url = this.resourceUrl + '?sort=id%2Cdesc&sort&page=6&size=1';
    return this.http.get<Country[]>(url, { observe: 'response' });
  }
}
