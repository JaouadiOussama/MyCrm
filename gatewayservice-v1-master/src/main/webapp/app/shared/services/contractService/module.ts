export interface BillItem {
  biDeleted: number;
  biDesc: string;
  biPu: number;
  biQuantity: number;
  biTotalAmount: number;
  biTva: number;
  createdAt: string;
  id: number;
  updatedAt: string;
}

export interface Currency {
  bills: any[];
  createdAt: string;
  currName: string;
  id: number;
  updatedAt: string;
}

export interface PaymentType {
  bills: any[];
  createdAt: string;
  id: number;
  payTypeName: string;
  updatedAt: string;
}

export interface Bill {
  billDate: string;
  billDeleted: number;
  billDueCond: string;
  billDueDate: string;
  billInfoCompte: string;
  billItems: BillItem[];
  billName: string;
  billPenality: number;
  billRef: string;
  createdAt: string;
  currency: Currency;
  id: number;
  paymentType: PaymentType;
  totalDiscountHt: number;
  totalHt: number;
  totalTtc: number;
  totalTva: number;
  updatedAt: string;
}

export interface Contact {
  contactId: number;
  contactName: string;
  contracts: any[];
  deleted: number;
  id: number;
}

export interface Company {
  compId: number;
  compName: string;
  contacts: Contact[];
  contracts: any[];
  deleted: number;
  id: number;
}

export interface ContractItem {
  ciDeleted: number;
  ciDesc: string;
  ciPu: number;
  ciQuantity: number;
  ciTva: number;
  createdAt: string;
  id: number;
  updatedAt: string;
}

export interface Contract {
  bills: Bill[];
  company: Company;
  contact: Contact;
  contractDeleted: string;
  contractEndDate: string;
  contractItems: ContractItem[];
  contractName: string;
  contractRef: string;
  contractReminderDate: string;
  contractSignedDate: string;
  contractStartDate: string;
  contractStatus: string;
  contractType: string;
  createdAt: string;
  id: number;
  updatedAt: string;
}
