import { Injectable } from '@angular/core';
import { HttpResponse, HttpClient } from '@angular/common/http';

import { SERVER_API_URL } from 'app/app.constants';
import { Observable } from 'rxjs';
import { createRequestOption } from 'app/shared/util/request-util';
import { Contract } from './module';

type EntityResponseType = HttpResponse<Contract>;
type EntityArrayResponseType = HttpResponse<Contract[]>;

@Injectable({
  providedIn: 'root'
})
export class ContractService {
  public resourceUrl = SERVER_API_URL + 'services/contractbillservice/api/contracts';

  constructor(private http: HttpClient) {}
  /* create Contract*/
  create(contract: Contract): Observable<EntityResponseType> {
    return this.http.post<Contract>(this.resourceUrl, contract, { observe: 'response' });
  }

  /* find by id */
  find(id: number): Observable<EntityResponseType> {
    return this.http.get<Contract>(`${this.resourceUrl}/${id}`, { observe: 'response' });
  }

  /* query */
  query(req?: any): Observable<EntityArrayResponseType> {
    const options = createRequestOption(req);
    return this.http.get<Contract[]>(this.resourceUrl, { params: options, observe: 'response' });
  }

  update(contract: Contract): Observable<EntityResponseType> {
    return this.http.put<Contract>(this.resourceUrl, contract, { observe: 'response' });
  }
}
