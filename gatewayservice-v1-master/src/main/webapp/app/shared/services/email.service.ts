import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';

import { HttpClient, HttpResponse } from '@angular/common/http';
import { SERVER_API_URL } from 'app/app.constants';

import { createRequestOption } from 'app/shared/util/request-util';

import { Email } from './model';
type EntityResponseType = HttpResponse<Email>;
type EntityArrayResponseType = HttpResponse<Email[]>;

@Injectable({ providedIn: 'root' })
export class EmailService {
  public resourceUrl = SERVER_API_URL + 'services/personmicroservice/api/emails';

  constructor(private http: HttpClient) {}
  /* create Email*/
  create(email: Email): Observable<EntityResponseType> {
    return this.http.post<Email>(this.resourceUrl, email, { observe: 'response' });
  }

  /* find by id */
  find(id: number): Observable<EntityResponseType> {
    return this.http.get<Email>(`${this.resourceUrl}/${id}`, { observe: 'response' });
  }

  /* query */
  query(req?: any): Observable<EntityArrayResponseType> {
    const options = createRequestOption(req);
    return this.http.get<Email[]>(this.resourceUrl, { params: options, observe: 'response' });
  }

  update(email: Email): Observable<EntityResponseType> {
    return this.http.put<Email>(this.resourceUrl, email, { observe: 'response' });
  }

  /* query */
  queryLastInsert(): Observable<EntityArrayResponseType> {
    const url = this.resourceUrl + '?sort=id%2Cdesc&sort&page=6&size=1';
    return this.http.get<Email[]>(url, { observe: 'response' });
  }
  /* find by deleted and personalInfo */
  findByDeletedAndInfo(deleted: number, personalInfo: number, req?: any): Observable<EntityArrayResponseType> {
    const options = createRequestOption(req);
    return this.http.get<Email[]>(`${this.resourceUrl}/deleted/${deleted}/info/${personalInfo}`, {
      params: options,
      observe: 'response'
    });
  }
}
