import { Injectable } from '@angular/core';
import { HttpResponse, HttpClient } from '@angular/common/http';
import { Phone } from './model';
import { SERVER_API_URL } from 'app/app.constants';
import { Observable } from 'rxjs';
import { createRequestOption } from 'app/shared/util/request-util';

type EntityResponseType = HttpResponse<Phone>;
type EntityArrayResponseType = HttpResponse<Phone[]>;

@Injectable({
  providedIn: 'root'
})
export class PhoneService {
  public resourceUrl = SERVER_API_URL + 'services/personmicroservice/api/phones';

  constructor(private http: HttpClient) {}
  /* create Phone*/
  create(phone: Phone): Observable<EntityResponseType> {
    return this.http.post<Phone>(this.resourceUrl, phone, { observe: 'response' });
  }

  /* find by id */
  find(id: number): Observable<EntityResponseType> {
    return this.http.get<Phone>(`${this.resourceUrl}/${id}`, { observe: 'response' });
  }

  /* query */
  query(req?: any): Observable<EntityArrayResponseType> {
    const options = createRequestOption(req);
    return this.http.get<Phone[]>(this.resourceUrl, { params: options, observe: 'response' });
  }

  /* update */

  update(phone: Phone): Observable<EntityResponseType> {
    return this.http.put<Phone>(this.resourceUrl, phone, { observe: 'response' });
  }

  /* find by deleted */
  findByDeleted(deleted: number, req?: any): Observable<EntityArrayResponseType> {
    const options = createRequestOption(req);
    return this.http.get<Phone[]>(`${this.resourceUrl}/deleted/${deleted}`, { params: options, observe: 'response' });
  }

  /* find by deleted and personalInfo */
  findByDeletedAndInfo(deleted: number, personalInfo: number, req?: any): Observable<EntityArrayResponseType> {
    const options = createRequestOption(req);
    return this.http.get<Phone[]>(`${this.resourceUrl}/deleted/${deleted}/info/${personalInfo}`, {
      params: options,
      observe: 'response'
    });
  }
}
