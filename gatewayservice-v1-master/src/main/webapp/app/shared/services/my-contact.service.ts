import { Injectable } from '@angular/core';
import { HttpResponse, HttpClient } from '@angular/common/http';
import { Contact } from './model';
import { SERVER_API_URL } from 'app/app.constants';
import { Observable } from 'rxjs';
import { createRequestOption } from 'app/shared/util/request-util';

type EntityResponseType = HttpResponse<Contact>;
type EntityArrayResponseType = HttpResponse<Contact[]>;

@Injectable({
  providedIn: 'root'
})
export class ContactService {
  public resourceUrl = SERVER_API_URL + 'services/personmicroservice/api/contacts';

  constructor(private http: HttpClient) {}
  /* create Contact*/
  create(contact: Contact): Observable<EntityResponseType> {
    return this.http.post<Contact>(this.resourceUrl, contact, { observe: 'response' });
  }

  /* find by id */
  find(id: number): Observable<EntityResponseType> {
    return this.http.get<Contact>(`${this.resourceUrl}/${id}`, { observe: 'response' });
  }

  /* query */
  query(req?: any): Observable<EntityArrayResponseType> {
    const options = createRequestOption(req);
    return this.http.get<Contact[]>(this.resourceUrl, { params: options, observe: 'response' });
  }

  update(contact: Contact): Observable<EntityResponseType> {
    return this.http.put<Contact>(this.resourceUrl, contact, { observe: 'response' });
  }

  /* find by deleted */
  findByDeleted(deleted: number, req?: any): Observable<EntityArrayResponseType> {
    const options = createRequestOption(req);
    return this.http.get<Contact[]>(`${this.resourceUrl}/deleted/${deleted}`, { params: options, observe: 'response' });
  }
}
