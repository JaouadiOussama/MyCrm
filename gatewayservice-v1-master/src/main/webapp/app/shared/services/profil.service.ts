import { Injectable } from '@angular/core';
import { HttpResponse, HttpClient } from '@angular/common/http';
import { Profil } from './model';
import { SERVER_API_URL } from 'app/app.constants';
import { Observable } from 'rxjs';
import { createRequestOption } from 'app/shared/util/request-util';

type EntityResponseType = HttpResponse<Profil>;
type EntityArrayResponseType = HttpResponse<Profil[]>;

@Injectable({
  providedIn: 'root'
})
export class ProfilService {
  public resourceUrl = SERVER_API_URL + 'services/personmicroservice/api/profils';

  constructor(private http: HttpClient) {}
  /* create Profil*/
  create(profil: Profil): Observable<EntityResponseType> {
    return this.http.post<Profil>(this.resourceUrl, profil, { observe: 'response' });
  }

  /* find by id */
  find(id: number): Observable<EntityResponseType> {
    return this.http.get<Profil>(`${this.resourceUrl}/${id}`, { observe: 'response' });
  }

  /* query */
  query(req?: any): Observable<EntityArrayResponseType> {
    const options = createRequestOption(req);
    return this.http.get<Profil[]>(this.resourceUrl, { params: options, observe: 'response' });
  }

  /* update */

  update(profil: Profil): Observable<EntityResponseType> {
    return this.http.put<Profil>(this.resourceUrl, profil, { observe: 'response' });
  }

  /* find by deleted */
  findByDeleted(deleted: number, req?: any): Observable<EntityArrayResponseType> {
    const options = createRequestOption(req);
    return this.http.get<Profil[]>(`${this.resourceUrl}/deleted/${deleted}`, { params: options, observe: 'response' });
  }

  /* find by deleted and personalInfo */
  findByDeletedAndPersonalInfo(deleted: number, personalInfo: number, req?: any): Observable<EntityArrayResponseType> {
    const options = createRequestOption(req);
    return this.http.get<Profil[]>(`${this.resourceUrl}/deleted/${deleted}/info/${personalInfo}`, {
      params: options,
      observe: 'response'
    });
  }
}
