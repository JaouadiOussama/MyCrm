import { Injectable } from '@angular/core';
import { NgbModal, NgbModalRef } from '@ng-bootstrap/ng-bootstrap';

import { AjoutcompteModalComponent } from 'app/my-account/compte-ajouter/compte-ajouter.component';

@Injectable({ providedIn: 'root' })
export class AjoutcompteModalService {
  private isOpen = false;

  constructor(private modalService: NgbModal) {}
  // open pop up
  open(): void {
    if (this.isOpen) {
      return;
    }
    this.isOpen = true;
    const modalRef: NgbModalRef = this.modalService.open(AjoutcompteModalComponent);
    modalRef.result.finally(() => (this.isOpen = false));
  }
}
