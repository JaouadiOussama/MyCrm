import { Injectable } from '@angular/core';
import { HttpResponse, HttpClient } from '@angular/common/http';
import { Employee } from './model';
import { SERVER_API_URL } from 'app/app.constants';
import { Observable } from 'rxjs';
import { createRequestOption } from 'app/shared/util/request-util';

type EntityResponseType = HttpResponse<Employee>;
type EntityArrayResponseType = HttpResponse<Employee[]>;

@Injectable({
  providedIn: 'root'
})
export class EmployeeService {
  public resourceUrl = SERVER_API_URL + 'services/personmicroservice/api/employees';

  constructor(private http: HttpClient) {}
  /* create Contact*/
  create(employee: Employee): Observable<EntityResponseType> {
    return this.http.post<Employee>(this.resourceUrl, employee, { observe: 'response' });
  }

  /* find by id */
  find(id: number): Observable<EntityResponseType> {
    return this.http.get<Employee>(`${this.resourceUrl}/${id}`, { observe: 'response' });
  }

  /* query */
  query(req?: any): Observable<EntityArrayResponseType> {
    const options = createRequestOption(req);
    return this.http.get<Employee[]>(this.resourceUrl, { params: options, observe: 'response' });
  }

  update(employee: Employee): Observable<EntityResponseType> {
    return this.http.put<Employee>(this.resourceUrl, employee, { observe: 'response' });
  }

  /* find by deleted */
  findByDeleted(deleted: number, req?: any): Observable<EntityArrayResponseType> {
    const options = createRequestOption(req);
    return this.http.get<Employee[]>(`${this.resourceUrl}/deleted/${deleted}`, { params: options, observe: 'response' });
  }
}
