import { Injectable } from '@angular/core';
import { HttpResponse, HttpClient } from '@angular/common/http';

import { SERVER_API_URL } from 'app/app.constants';
import { Observable } from 'rxjs';
import { createRequestOption } from 'app/shared/util/request-util';
import { Opportunity } from './module';

type EntityResponseType = HttpResponse<Opportunity>;
type EntityArrayResponseType = HttpResponse<Opportunity[]>;

@Injectable({
  providedIn: 'root'
})
export class OpportunityService {
  public resourceUrl = SERVER_API_URL + 'services/projetservice/api/opportunities';

  constructor(private http: HttpClient) {}
  /* create Opportunity*/
  create(opportunity: Opportunity): Observable<EntityResponseType> {
    return this.http.post<Opportunity>(this.resourceUrl, opportunity, { observe: 'response' });
  }

  /* find by id */
  find(id: number): Observable<EntityResponseType> {
    return this.http.get<Opportunity>(`${this.resourceUrl}/${id}`, { observe: 'response' });
  }

  /* query */
  query(req?: any): Observable<EntityArrayResponseType> {
    const options = createRequestOption(req);
    return this.http.get<Opportunity[]>(this.resourceUrl, { params: options, observe: 'response' });
  }

  update(opportunity: Opportunity): Observable<EntityResponseType> {
    return this.http.put<Opportunity>(this.resourceUrl, opportunity, { observe: 'response' });
  }

  /* find by deleted */
  findByDeleted(deleted: number, req?: any): Observable<EntityArrayResponseType> {
    const options = createRequestOption(req);
    return this.http.get<Opportunity[]>(`${this.resourceUrl}/deleted/${deleted}`, { params: options, observe: 'response' });
  }
}
