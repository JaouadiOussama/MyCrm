import { Injectable } from '@angular/core';
import { HttpResponse, HttpClient } from '@angular/common/http';

import { SERVER_API_URL } from 'app/app.constants';
import { Observable } from 'rxjs';
import { createRequestOption } from 'app/shared/util/request-util';
import { Stage } from './module';

type EntityResponseType = HttpResponse<Stage>;
type EntityArrayResponseType = HttpResponse<Stage[]>;

@Injectable({
  providedIn: 'root'
})
export class StageService {
  public resourceUrl = SERVER_API_URL + 'services/projetservice/api/stages';

  constructor(private http: HttpClient) {}
  /* create Stage*/
  create(stage: Stage): Observable<EntityResponseType> {
    return this.http.post<Stage>(this.resourceUrl, stage, { observe: 'response' });
  }

  /* find by id */
  find(id: number): Observable<EntityResponseType> {
    return this.http.get<Stage>(`${this.resourceUrl}/${id}`, { observe: 'response' });
  }

  /* query */
  query(req?: any): Observable<EntityArrayResponseType> {
    const options = createRequestOption(req);
    return this.http.get<Stage[]>(this.resourceUrl, { params: options, observe: 'response' });
  }

  update(stage: Stage): Observable<EntityResponseType> {
    return this.http.put<Stage>(this.resourceUrl, stage, { observe: 'response' });
  }
}
