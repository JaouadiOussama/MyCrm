export interface CompanyOpportunity {
  id?: number;
  idPersonService?: number;
  compName?: string;
  opportunities?: any[];
}
export interface ContactOpportunity {
  id?: number;
  idPersonService?: number;
  compName?: string;

  opportunities?: any[];
}

export interface Currency {
  createdAt?: string;
  currName?: string;
  deleted?: number;
  id?: number;
  opportunities?: any[];
  updatedAt?: string;
}

export interface Stage {
  createdAt?: string;
  deleted?: number;
  id?: number;
  stageName?: string;
  stageProbability?: number;
  updatedAt?: string;
}

export interface Pipeline {
  createdAt?: string;
  deleted?: number;
  id?: number;
  pipeName?: string;
  stages?: Stage[];
  updatedAt?: string;
}

export interface PricingType {
  createdAt?: string;
  deleted?: number;
  id?: number;
  opportunities?: any[];
  pricingTypeName?: string;
  updatedAt?: string;
}

export interface Opportunity {
  company?: CompanyOpportunity;
  contact?: ContactOpportunity;
  createdAt?: Date;
  currency?: Currency;
  deleted?: number;
  id?: number;
  oppClosedate?: string;
  oppDesc?: string;
  oppLeadOrigin?: string;
  oppName?: string;
  oppPertCause?: string;
  oppTurnover?: number;
  oppType?: string;
  oppValue?: number;
  stage?: Stage;
  pricingType?: PricingType;
  projEndate?: string;
  projOpp?: number;
  projPriority?: string;
  projStartdate?: string;
  updatedAt?: Date;
}

export class COpportunity implements Opportunity {
  constructor(
    public company?: CompanyOpportunity,
    public contact?: ContactOpportunity,
    public createdAt?: Date,
    public currency?: Currency,
    public deleted?: number,
    public id?: number,
    public oppClosedate?: string,
    public oppDesc?: string,
    public oppLeadOrigin?: string,
    public oppName?: string,
    public oppPertCause?: string,
    public oppTurnover?: number,
    public oppType?: string,
    public oppValue?: number,
    public stage?: Stage,
    public pricingType?: PricingType,
    public projEndate?: string,
    public projOpp?: number,
    public projPriority?: string,
    public projStartdate?: string,
    public updatedAt?: Date
  ) {}
}
