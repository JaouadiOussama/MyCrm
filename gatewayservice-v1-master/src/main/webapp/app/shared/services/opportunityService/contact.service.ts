import { Injectable } from '@angular/core';
import { HttpResponse, HttpClient } from '@angular/common/http';

import { SERVER_API_URL } from 'app/app.constants';
import { Observable } from 'rxjs';
import { createRequestOption } from 'app/shared/util/request-util';
import { ContactOpportunity } from './module';

type EntityResponseType = HttpResponse<ContactOpportunity>;
type EntityArrayResponseType = HttpResponse<ContactOpportunity[]>;

@Injectable({
  providedIn: 'root'
})
export class ContactOpportunityService {
  public resourceUrl = SERVER_API_URL + 'services/projetservice/api/contact-opportunities';

  constructor(private http: HttpClient) {}
  /* create ContactOpportunity*/
  create(company: ContactOpportunity): Observable<EntityResponseType> {
    return this.http.post<ContactOpportunity>(this.resourceUrl, company, { observe: 'response' });
  }

  /* find by id */
  find(id: number): Observable<EntityResponseType> {
    return this.http.get<ContactOpportunity>(`${this.resourceUrl}/${id}`, { observe: 'response' });
  }

  /* query */
  query(req?: any): Observable<EntityArrayResponseType> {
    const options = createRequestOption(req);
    return this.http.get<ContactOpportunity[]>(this.resourceUrl, { params: options, observe: 'response' });
  }

  update(company: ContactOpportunity): Observable<EntityResponseType> {
    return this.http.put<ContactOpportunity>(this.resourceUrl, company, { observe: 'response' });
  }
}
