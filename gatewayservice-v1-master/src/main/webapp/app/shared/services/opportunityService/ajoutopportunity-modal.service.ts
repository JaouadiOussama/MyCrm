import { Injectable } from '@angular/core';
import { NgbModal, NgbModalRef } from '@ng-bootstrap/ng-bootstrap';
import { AjoutOpportunityModalComponent } from 'app/my-sale/my-opportunites/opportunity-ajout/opportunity-ajout.component';

@Injectable({ providedIn: 'root' })
export class AjoutOpportunityModalService {
  private isOpen = false;

  constructor(private modalService: NgbModal) {}

  open(): void {
    if (this.isOpen) {
      return;
    }
    this.isOpen = true;
    const modalRef: NgbModalRef = this.modalService.open(AjoutOpportunityModalComponent);
    modalRef.result.finally(() => (this.isOpen = false));
  }
}
