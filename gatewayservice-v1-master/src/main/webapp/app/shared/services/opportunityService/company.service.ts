import { Injectable } from '@angular/core';
import { HttpResponse, HttpClient } from '@angular/common/http';

import { SERVER_API_URL } from 'app/app.constants';
import { Observable } from 'rxjs';
import { createRequestOption } from 'app/shared/util/request-util';
import { CompanyOpportunity } from './module';

type EntityResponseType = HttpResponse<CompanyOpportunity>;
type EntityArrayResponseType = HttpResponse<CompanyOpportunity[]>;

@Injectable({
  providedIn: 'root'
})
export class CompanyOpportunityService {
  public resourceUrl = SERVER_API_URL + 'services/projetservice/api/companies';

  constructor(private http: HttpClient) {}
  /* create CompanyOpportunity*/
  create(company: CompanyOpportunity): Observable<EntityResponseType> {
    return this.http.post<CompanyOpportunity>(this.resourceUrl, company, { observe: 'response' });
  }

  /* find by id */
  find(id: number): Observable<EntityResponseType> {
    return this.http.get<CompanyOpportunity>(`${this.resourceUrl}/${id}`, { observe: 'response' });
  }

  /* query */
  query(req?: any): Observable<EntityArrayResponseType> {
    const options = createRequestOption(req);
    return this.http.get<CompanyOpportunity[]>(this.resourceUrl, { params: options, observe: 'response' });
  }

  update(company: CompanyOpportunity): Observable<EntityResponseType> {
    return this.http.put<CompanyOpportunity>(this.resourceUrl, company, { observe: 'response' });
  }
}
