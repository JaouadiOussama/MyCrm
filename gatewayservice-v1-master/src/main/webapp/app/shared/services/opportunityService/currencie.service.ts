import { Injectable } from '@angular/core';
import { HttpResponse, HttpClient } from '@angular/common/http';

import { SERVER_API_URL } from 'app/app.constants';
import { Observable } from 'rxjs';
import { createRequestOption } from 'app/shared/util/request-util';
import { Currency } from './module';

type EntityResponseType = HttpResponse<Currency>;
type EntityArrayResponseType = HttpResponse<Currency[]>;

@Injectable({
  providedIn: 'root'
})
export class CurrencyService {
  public resourceUrl = SERVER_API_URL + 'services/projetservice/api/currencies';

  constructor(private http: HttpClient) {}
  /* create Currency*/
  create(currency: Currency): Observable<EntityResponseType> {
    return this.http.post<Currency>(this.resourceUrl, currency, { observe: 'response' });
  }

  /* find by id */
  find(id: number): Observable<EntityResponseType> {
    return this.http.get<Currency>(`${this.resourceUrl}/${id}`, { observe: 'response' });
  }

  /* query */
  query(req?: any): Observable<EntityArrayResponseType> {
    const options = createRequestOption(req);
    return this.http.get<Currency[]>(this.resourceUrl, { params: options, observe: 'response' });
  }

  update(currency: Currency): Observable<EntityResponseType> {
    return this.http.put<Currency>(this.resourceUrl, currency, { observe: 'response' });
  }
}
