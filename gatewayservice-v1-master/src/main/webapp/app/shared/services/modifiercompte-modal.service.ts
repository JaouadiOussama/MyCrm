import { Injectable } from '@angular/core';
import { NgbModal, NgbModalRef } from '@ng-bootstrap/ng-bootstrap';
import { ModifiercompteModalComponent } from 'app/my-account/account-modifier/account-modifier.component';

@Injectable({ providedIn: 'root' })
export class ModifiercompteModalService {
  private isOpen = false;

  constructor(private modalService: NgbModal) {}
  // open pop up
  open(): void {
    if (this.isOpen) {
      return;
    }
    this.isOpen = true;
    const modalRef: NgbModalRef = this.modalService.open(ModifiercompteModalComponent);
    modalRef.result.finally(() => (this.isOpen = false));
  }
}
