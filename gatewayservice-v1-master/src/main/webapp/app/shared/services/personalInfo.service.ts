import { Injectable } from '@angular/core';
import { HttpResponse, HttpClient } from '@angular/common/http';
import { Country, PersonalInfo } from './model';
import { SERVER_API_URL } from 'app/app.constants';
import { Observable } from 'rxjs';
import { createRequestOption } from '../util/request-util';

type EntityResponseType1 = HttpResponse<Country>;
type EntityResponseType2 = HttpResponse<PersonalInfo>;
type EntityArrayResponseType = HttpResponse<PersonalInfo[]>;

@Injectable({
  providedIn: 'root'
})
export class PersonalInfoService {
  public resourceUrl = SERVER_API_URL + 'services/personmicroservice/api/personal-infos';

  constructor(private http: HttpClient) {}
  /* create PersonalInfo*/
  create(personalInfo: PersonalInfo): Observable<EntityResponseType2> {
    return this.http.post<PersonalInfo>(this.resourceUrl, personalInfo, { observe: 'response' });
  }

  /* find by id */
  find(id: number): Observable<EntityResponseType2> {
    return this.http.get<PersonalInfo>(`${this.resourceUrl}/${id}`, { observe: 'response' });
  }

  /* query */
  query(req?: any): Observable<EntityArrayResponseType> {
    const options = createRequestOption(req);
    return this.http.get<PersonalInfo[]>(this.resourceUrl, { params: options, observe: 'response' });
  }

  /* update */

  update(personalInfo: PersonalInfo): Observable<EntityResponseType2> {
    return this.http.put<PersonalInfo>(this.resourceUrl, personalInfo, { observe: 'response' });
  }
  /* get country  by personalInfo id */
  getCountry(id: number): Observable<EntityResponseType1> {
    return this.http.get<Country>(`${this.resourceUrl}/${id}/country`, { observe: 'response' });
  }
  /* find by deleted */
  findByDeleted(deleted: number, req?: any): Observable<EntityArrayResponseType> {
    const options = createRequestOption(req);
    return this.http.get<PersonalInfo[]>(`${this.resourceUrl}/deleted/${deleted}`, { params: options, observe: 'response' });
  }

  /* find by deleted and company */
  findByDeletedAndCompany(deleted: number, company: number, req?: any): Observable<EntityArrayResponseType> {
    const options = createRequestOption(req);
    return this.http.get<PersonalInfo[]>(`${this.resourceUrl}/deleted/${deleted}/info/${company}`, {
      params: options,
      observe: 'response'
    });
  }
}
