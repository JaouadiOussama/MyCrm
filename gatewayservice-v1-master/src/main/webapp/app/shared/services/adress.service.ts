import { Injectable } from '@angular/core';
import { HttpResponse, HttpClient } from '@angular/common/http';
import { Country, Adress } from './model';
import { SERVER_API_URL } from 'app/app.constants';
import { Observable } from 'rxjs';
import { createRequestOption } from '../util/request-util';

type EntityResponseType = HttpResponse<Adress>;
type EntityArrayResponseType = HttpResponse<Adress[]>;

@Injectable({
  providedIn: 'root'
})
export class AdressService {
  public resourceUrl = SERVER_API_URL + 'services/personmicroservice/api/adresses';

  constructor(private http: HttpClient) {}
  /* create Adress*/
  create(adress: Adress): Observable<EntityResponseType> {
    return this.http.post<Adress>(this.resourceUrl, adress, { observe: 'response' });
  }

  /* find by id */
  find(id: number): Observable<EntityResponseType> {
    return this.http.get<Adress>(`${this.resourceUrl}/${id}`, { observe: 'response' });
  }

  /* query */
  query(req?: any): Observable<EntityArrayResponseType> {
    const options = createRequestOption(req);
    return this.http.get<Adress[]>(this.resourceUrl, { params: options, observe: 'response' });
  }

  /* update */

  update(adress: Adress): Observable<EntityResponseType> {
    return this.http.put<Adress>(this.resourceUrl, adress, { observe: 'response' });
  }
  /* get country  by Adress id
  getCountry(id: number): Observable<EntityResponseType1> {
    return this.http.get<Country>(`${this.resourceUrl}/${id}/country`, { observe: 'response' });
  }
  */

  /* find by deleted */
  findByDeleted(deleted: number, req?: any): Observable<EntityArrayResponseType> {
    const options = createRequestOption(req);
    return this.http.get<Adress[]>(`${this.resourceUrl}/deleted/${deleted}`, { params: options, observe: 'response' });
  }

  /* find by deleted and info */
  findByDeletedAndInfo(deleted: number, info: number, req?: any): Observable<EntityArrayResponseType> {
    const options = createRequestOption(req);
    return this.http.get<Adress[]>(`${this.resourceUrl}/deleted/${deleted}/info/${info}`, {
      params: options,
      observe: 'response'
    });
  }
}
