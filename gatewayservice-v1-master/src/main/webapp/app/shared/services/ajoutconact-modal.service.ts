import { Injectable } from '@angular/core';
import { NgbModal, NgbModalRef } from '@ng-bootstrap/ng-bootstrap';

import { AjoutcontactModalComponent } from 'app/my-contact/contact-ajouter/contact-ajouter.component';

@Injectable({ providedIn: 'root' })
export class AjoutcontactModalService {
  private isOpen = false;

  constructor(private modalService: NgbModal) {}

  open(): void {
    if (this.isOpen) {
      return;
    }
    this.isOpen = true;
    const modalRef: NgbModalRef = this.modalService.open(AjoutcontactModalComponent);
    modalRef.result.finally(() => (this.isOpen = false));
  }
}
