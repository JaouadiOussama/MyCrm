import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { HttpClient, HttpResponse } from '@angular/common/http';
import { SERVER_API_URL } from 'app/app.constants';
import { ActivityArea } from './model';
import { createRequestOption } from 'app/shared/util/request-util';

type EntityResponseType = HttpResponse<ActivityArea>;
type EntityArrayResponseType = HttpResponse<ActivityArea[]>;

@Injectable({ providedIn: 'root' })
export class ActivityService {
  public resourceUrl = SERVER_API_URL + 'services/personmicroservice/api/activity-areas';

  constructor(private http: HttpClient) {}
  /* create country*/
  // eslint-disable-next-line @typescript-eslint/tslint/config
  create(country: ActivityArea): Observable<EntityResponseType> {
    return this.http.post<ActivityArea>(this.resourceUrl, country, { observe: 'response' });
  }

  /* find by id */
  find(id: number): Observable<EntityResponseType> {
    return this.http.get<ActivityArea>(`${this.resourceUrl}/${id}`, { observe: 'response' });
  }

  /* query */
  query(req?: any): Observable<EntityArrayResponseType> {
    const options = createRequestOption(req);
    return this.http.get<ActivityArea[]>(this.resourceUrl, { params: options, observe: 'response' });
  }

  update(country: ActivityArea): Observable<EntityResponseType> {
    return this.http.put<ActivityArea>(this.resourceUrl, country, { observe: 'response' });
  }

  /* query */

  queryLastInsert(): Observable<EntityArrayResponseType> {
    const url = this.resourceUrl + '?sort=id%2Cdesc&sort&page=6&size=1';
    return this.http.get<ActivityArea[]>(url, { observe: 'response' });
  }
}
