export interface Adress {
  adr1?: string;
  adr2?: string;
  adrType?: string;
  city?: City;
  codeZip?: string;
  createdAt?: string;
  deleted?: number;
  id?: number;
  info?: Info;
  updatedAt?: string;
}

export class AAdress implements Adress {
  constructor(
    public adr1?: string,
    public adr2?: string,
    public adrType?: string,
    public city?: City,
    public codeZip?: string,
    public createdAt?: string,
    public deleted?: number,
    public id?: number,
    public info?: Info,
    public updatedAt?: string
  ) {}
}

export interface City {
  cityName?: string;
  createdAt?: string;
  enabled?: string;
  id?: number;
  updatedAt?: string;
  personalInfos?: any[];
  country?: Country;
}

export interface Country {
  cities?: City[];
  codePhone?: string;
  countName?: string;
  createdAt?: string;
  enabled?: string;
  id?: number;
  iso3?: string;
  personalInfos?: any[];
  updatedAt?: string;
}

export interface Email {
  createdAt?: string;
  email?: string;
  id?: number;
  emailType?: string;
  deleated?: number;
  updatedAt?: string;
  info?: Info;
}
export interface Employee {
  id?: number;
  empDateJoin?: string;
  empDateDetached?: string;
  salary?: number;
  persFonction?: string;
  persLastName: string;
  company?: Company;
}
export interface Info {
  createdAt?: string;
  id?: number;
  infoDesc?: string;
  infoName?: string;
  personalInfos?: any[];
  profils?: any[];
  status?: boolean;
  updatedAt?: string;
}

export interface Phone {
  createdAt?: string;
  id?: number;
  phoneNumber?: number;
  updatedAt?: string;
  deleted?: number;
  phoneType?: string;
  info?: Info; // create update phone
}

export interface PersonalInfo {
  adr1?: string;
  adr2?: string;
  codeZip?: number;
  country?: Country;
  city?: City;
  createdAt?: string;
  emails?: Email[];
  id?: number;
  info?: Company;
  phones?: Phone[];
  updatedAt?: string;
  deleted?: number;
  adrType?: string;
}

export interface Profil {
  createdAt?: string;
  id?: number;
  info?: Info;
  profil?: string;
  updatedAt?: string;
  url?: string;
  deleted?: number;
}

export interface Person {
  createdAt?: string;
  id?: number;
  infoDesc?: string;
  infoName?: string;
  persGender?: string;
  persLastName?: string;
  personBirthDate?: string;
  personalInfos?: PersonalInfo[];
  persFonction?: string;
  profils?: Profil[];
  status?: boolean;
  updatedAt?: string;
}

export interface Company {
  compNbEmp?: number;
  compRcs?: number;
  compSiren?: number;
  compSiret?: string;
  compSocForm?: string;
  compTva?: string;
  compType?: string;
  compYearCre?: number;
  createdAt?: string;
  id?: number;
  infoDesc?: string;
  infoName?: string;
  parentCompany?: any[];
  people?: Contact[];
  personalInfos?: PersonalInfo[];
  profils?: Profil[];
  status?: boolean;
  activityarea?: ActivityArea;
  updatedAt?: string;
  deleted?: number;
  comLogo?: string;
  emails?: Email[];
  phones?: Phone[];
  adresses?: Adress[];
  compNbEmb?: number;
}
export interface ActivityArea {
  id?: number;
  actiAreaName?: string;
}

export class Contact {
  adresses?: Adress[];
  contactLeadOrigin?: string;
  createdAt?: string;
  deleted?: number;
  emails?: Email[];
  id?: number;
  infoDesc?: string;
  infoName?: string;
  perFonction?: string;
  persBirthDate?: string;
  persDepartement?: string;
  persLastName?: string;
  persPhoto?: string;
  company?: Company;
  perssGender?: string;
  phones?: Phone[];
  profils?: Profil[];
  status?: boolean;
  updatedAt?: string;
}

export class CCompany implements Company {
  constructor(
    public compNbEmp?: number,
    public compRcs?: number,
    public compSiren?: number,
    public compSiret?: string,
    public compSocForm?: string,
    public compTva?: string,
    public compType?: string,
    public comYearCre?: number,
    public createdAt?: string,
    public id?: number,
    public infoDesc?: string,
    public infoName?: string,
    public parentCompany?: any[],
    public people?: Contact[],
    public personalInfos?: PersonalInfo[],
    public profils?: Profil[],
    public status?: boolean,
    public activityarea?: ActivityArea,
    public updatedAt?: string,
    public deleted?: number,
    public comLogo?: string,
    public emails?: Email[],
    public phones?: Phone[],
    public adresses?: Adress[],
    public compNbEmb?: number
  ) {}
}
export class CContact implements Contact {
  constructor(
    public adresses?: Adress[],
    public contactLeadOrigin?: string,
    public createdAt?: string,
    public deleted?: number,
    public emails?: Email[],
    public id?: number,
    public infoDesc?: string,
    public infoName?: string,
    public perFonction?: string,
    public persBirthDate?: string,
    public persDepartement?: string,
    public persLastName?: string,
    public persPhoto?: string,
    public company?: Company,
    public perssGender?: string,
    public phones?: Phone[],
    public profils?: Profil[],
    public status?: boolean,
    public updatedAt?: string
  ) {}
}

export class CEmail implements Email {
  constructor(
    public createdAt?: string,
    public email?: string,
    public id?: number,
    public emailType?: string,
    public deleated?: number,
    public updatedAt?: string,
    public info?: Info
  ) {}
}

export class CPhone implements Phone {
  constructor(
    public createdAt?: string,
    public id?: number,
    public phoneNumber?: number,
    public updatedAt?: string,
    public deleted?: number,
    public phoneType?: string,
    public info?: Info
  ) {}
}

export class CProfil implements Profil {
  constructor(
    public createdAt?: string,
    public id?: number,
    public info?: Info,
    public profil?: string,
    public updatedAt?: string,
    public url?: string,
    public deleted?: number
  ) {}
}

export class CPersonalInfo implements PersonalInfo {
  constructor(
    public adr1?: string,
    public adr2?: string,
    public codeZip?: number,
    public country?: Country,
    public city?: City,
    public createdAt?: string,
    public emails?: Email[],
    public id?: number,
    public info?: Company,
    public phones?: Phone[],
    public updatedAt?: string,
    public deleted?: number,
    public adrType?: string
  ) {}
}
