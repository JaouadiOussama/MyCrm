import { Component, OnInit, OnDestroy, Input } from '@angular/core';

import { Subscription } from 'rxjs';
import { JhiEventManager } from 'ng-jhipster';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { HttpResponse, HttpClient } from '@angular/common/http';
import { AdresseDeleteComponent } from './adresse-delete/adresse-delete.component';
import { AdresseUpdateComponent } from './adresse-update/adresse-update.component';

import { AdressService } from 'app/shared/services/adress.service';
import { Adress } from 'app/shared/services/model';

// declare variable

@Component({
  selector: 'jhi-adresse',
  templateUrl: './adresse.component.html',
  styleUrls: ['./adresse.component.scss']
})
export class AdresseComponent implements OnInit, OnDestroy {
  adresses?: Adress[];
  eventSubscriber?: Subscription;
  coordonne = true;
  @Input() InfoId!: number;

  constructor(
    protected adresseService: AdressService,
    protected eventManager: JhiEventManager,
    protected modalService: NgbModal,
    private http: HttpClient
  ) {}

  loadAll(): void {
    this.adresseService.findByDeletedAndInfo(0, this.InfoId).subscribe((res: HttpResponse<Adress[]>) => (this.adresses = res.body || []));
  }

  ngOnInit(): void {
    this.loadAll();
    this.registerChangeIn();
  }
  getAdresse(adresse: Adress): String {
    const adr =
      adresse.adr1 + ', ' + adresse.adr2 + ', ' + adresse.codeZip + ', ' + adresse.city?.cityName + ',' + adresse.city?.country?.countName;

    return adr;
  }

  ngOnDestroy(): void {
    if (this.eventSubscriber) {
      this.eventManager.destroy(this.eventSubscriber);
    }
  }

  trackId(index: number, item: Adress): number {
    // eslint-disable-next-line @typescript-eslint/no-unnecessary-type-assertion
    return item.id!;
  }

  registerChangeIn(): void {
    this.eventSubscriber = this.eventManager.subscribe('AdresseModification', () => this.loadAll());
  }

  delete(adresse: Adress): void {
    const modalRef = this.modalService.open(AdresseDeleteComponent, { size: 'md', backdrop: 'static' });
    modalRef.componentInstance.adresse = adresse;
  }
  update(adresse?: Adress): void {
    const modalRef = this.modalService.open(AdresseUpdateComponent, { size: 'md', backdrop: 'static' });
    if (adresse) {
      modalRef.componentInstance.adresse = adresse;
      modalRef.componentInstance.InfoId = this.InfoId;
      modalRef.componentInstance.create = false;
    } else {
      modalRef.componentInstance.adresse = adresse;
      modalRef.componentInstance.InfoId = this.InfoId;

      modalRef.componentInstance.create = true;
    }
  }
}
