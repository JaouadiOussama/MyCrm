import { Component, OnInit } from '@angular/core';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { PersonalInfoService } from 'app/shared/services/personalInfo.service';
import { PersonalInfo, CPersonalInfo, Adress, AAdress } from 'app/shared/services/model';
import { ToastrService } from 'ngx-toastr';
import { JhiEventManager } from 'ng-jhipster';
import { HttpResponse } from '@angular/common/http';
import { TranslateService } from '@ngx-translate/core';
import { AdressService } from 'app/shared/services/adress.service';
import { Address } from 'cluster';

@Component({
  selector: 'jhi-adresse-delete',
  templateUrl: './adresse-delete.component.html',
  styleUrls: ['./adresse-delete.component.scss']
})
export class AdresseDeleteComponent implements OnInit {
  adresse!: Adress;

  constructor(
    protected adresseService: AdressService,
    public activeModal: NgbActiveModal,
    protected toastr: ToastrService,
    protected eventManager: JhiEventManager,
    public translateService: TranslateService
  ) {}
  ngOnInit(): void {}
  cancel(): void {
    this.activeModal.dismiss();
  }

  confirmDelete(id: number): void {
    this.adresseService.find(id).subscribe((res: HttpResponse<Adress>) => {
      const entityNameSupp = this.translateService.instant('adresse.message.supprimerSuccess');
      this.adresse = res.body || new AAdress();
      this.adresse.deleted = 1;
      this.adresseService.update(this.adresse).subscribe(() => {
        this.toastr.success(entityNameSupp);
        this.eventManager.broadcast('AdresseModification');
        this.activeModal.close();
      });
    });
  }
}
