import { Resolve, Router, ActivatedRouteSnapshot, Routes } from '@angular/router';
import { CPersonalInfo, PersonalInfo } from 'app/shared/services/model';

import { Injectable } from '@angular/core';
import { Observable, of, EMPTY } from 'rxjs';
import { flatMap } from 'rxjs/operators';
import { HttpResponse } from '@angular/common/http';

import { Authority } from 'app/shared/constants/authority.constants';
import { UserRouteAccessService } from 'app/core/auth/user-route-access-service';
import { AdresseUpdateComponent } from './adresse-update/adresse-update.component';
import { PersonalInfoService } from '../services/personalInfo.service';

@Injectable({ providedIn: 'root' })
export class AdresseResolve implements Resolve<PersonalInfo> {
  constructor(private service: PersonalInfoService, private router: Router) {}

  resolve(route: ActivatedRouteSnapshot): Observable<PersonalInfo> | Observable<never> {
    const id = route.params['id'];
    if (id) {
      return this.service.find(id).pipe(
        flatMap((adresse: HttpResponse<PersonalInfo>) => {
          if (adresse.body) {
            return of(adresse.body);
          } else {
            this.router.navigate(['404']);
            return EMPTY;
          }
        })
      );
    }
    return of(new CPersonalInfo());
  }
}

export const adresseRoute: Routes = [
  {
    path: 'adresse/new',
    component: AdresseUpdateComponent,
    resolve: {
      email: AdresseResolve
    },
    data: {
      authorities: [Authority.USER],
      pageTitle: 'adresses'
    },
    canActivate: [UserRouteAccessService]
  },

  {
    path: 'adresse/:id/edit',
    component: AdresseUpdateComponent,
    resolve: {
      adresse: AdresseResolve
    },
    data: {
      authorities: [Authority.USER],
      pageTitle: 'adresses'
    },
    canActivate: [UserRouteAccessService]
  }
];
