import { Component, OnInit, OnDestroy } from '@angular/core';
import { Adress, Company, CPersonalInfo, Country, City, Info, AAdress } from 'app/shared/services/model';
import { Validators, FormBuilder } from '@angular/forms';

import { ActivatedRoute } from '@angular/router';
import { ToastrService } from 'ngx-toastr';
import { JhiEventManager } from 'ng-jhipster';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { HttpResponse } from '@angular/common/http';
import { Observable, Subscription } from 'rxjs';
import { CountryService } from 'app/shared/services/country.service';

import { TranslateService } from '@ngx-translate/core';
import { isNumber } from 'util';
import { AdressService } from 'app/shared/services/adress.service';

@Component({
  selector: 'jhi-adresse-update',
  templateUrl: './adresse-update.component.html',
  styleUrls: ['./adresse-update.component.scss']
})
export class AdresseUpdateComponent implements OnInit, OnDestroy {
  isSaving = false;
  adresse?: Adress;
  create = false;
  InfoId = 0;
  countries: Country[] = [];
  states!: City[];
  selectedCity: any;
  selected: any;

  companies?: Company[];
  httpErrorListener!: Subscription;
  type = [{ name: 'Biling address' }, { name: 'Shipping address' }, { name: 'Purchase address' }];

  editForm = this.fb.group({
    id: [],
    adr1: [null, [Validators.required]],
    adr2: [''],
    codeZip: [''],
    country: [''],
    city: [''],
    adresseType: [null, [Validators.required]]
  });

  constructor(
    protected adresssService: AdressService,
    protected activatedRoute: ActivatedRoute,
    private fb: FormBuilder,
    protected toastr: ToastrService,
    protected countryService: CountryService,
    protected eventManager: JhiEventManager,
    public activeModal: NgbActiveModal,
    public translateService: TranslateService
  ) {}

  cancel(): void {
    this.activeModal.dismiss();
  }
  loadAll(): void {
    this.countryService
      .query({
        size: 248
      })
      .subscribe((res: HttpResponse<Country[]>) => (this.countries = res.body || []));
  }
  ngOnInit(): void {
    this.loadAll();
    if (this.adresse !== undefined) {
      this.updateForm(this.adresse);
    }
  }
  onSelect(): void {
    this.editForm.get(['city'])?.reset();
    if (this.selectedCity !== null) {
      this.countryService.findCities(this.selectedCity).subscribe((res: HttpResponse<any>) => (this.states = res.body || []));
    }
  }

  onSelectCity(): void {}

  updateForm(adresse: Adress): void {
    this.editForm.patchValue({
      id: adresse.id,
      adr1: adresse.adr1,
      adr2: adresse.adr2,
      codeZip: adresse.codeZip,
      country: adresse.city?.country?.countName,
      city: adresse.city?.cityName,
      adresseType: adresse.adrType
    });

    this.selected = adresse.city?.cityName;
    this.selectedCity = adresse.city?.country?.countName;
  }

  previousState(): void {
    window.history.back();
  }
  // convenience getter for easy access to form fields
  // eslint-disable-next-line @typescript-eslint/tslint/config
  get f() {
    return this.editForm.controls;
  }

  save(): void {
    this.isSaving = true;
    const adresse = this.createFromForm();
    if (this.create === false) {
      this.subscribeToSaveResponse(this.adresssService.update(adresse));
    } else {
      this.subscribeToSaveResponse(this.adresssService.create(adresse));
    }
  }
  private createFromForm(): Adress {
    const adresse = new AAdress();

    adresse.id = this.editForm.get(['id'])!.value;
    adresse.adr1 = this.editForm.get(['adr1'])!.value;
    adresse.adr2 = this.editForm.get(['adr2'])!.value;
    adresse.codeZip = this.editForm.get(['codeZip'])!.value;
    adresse.adrType = this.editForm.get(['adresseType'])!.value;
    adresse.deleted = 0;
    adresse.info = this.createInfo();
    if (this.editForm.get(['city'])!.value) {
      adresse.city = this.createCity();
    }

    return adresse;
  }
  private createInfo(): Info {
    return {
      id: this.InfoId
    };
  }
  private createCity(): City {
    if (!isNumber(this.selected)) {
      this.selected = this.adresse?.city?.id;
    }
    return {
      id: this.selected
    };
  }
  private createCountry(): Country {
    return {
      id: this.selectedCity
    };
  }

  protected subscribeToSaveResponse(result: Observable<HttpResponse<Adress>>): void {
    result.subscribe(
      () => this.onSaveSuccess(),
      () => this.onSaveError()
    );
  }

  protected onSaveSuccess(): void {
    this.isSaving = false;
    const entityNameCreated = this.translateService.instant('adresse.message.create');
    const entityNameUpdate = this.translateService.instant('adresse.message.update');

    if (this.create) {
      this.toastr.success(entityNameCreated);
    } else {
      this.toastr.success(entityNameUpdate);
    }

    this.eventManager.broadcast('AdresseModification');
    this.activeModal.close();
  }

  protected onSaveError(): void {
    this.isSaving = false;
  }
  ngOnDestroy(): void {
    if (this.httpErrorListener) {
      this.eventManager.destroy(this.httpErrorListener);
    }
  }
}
