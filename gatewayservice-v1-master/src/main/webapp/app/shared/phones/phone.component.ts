import { Component, OnInit, OnDestroy, Input } from '@angular/core';
import { Phone, Country } from 'app/shared/services/model';
import { Subscription } from 'rxjs';

import { JhiEventManager } from 'ng-jhipster';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { HttpResponse } from '@angular/common/http';
import { PhoneService } from 'app/shared/services/phone.service';
import { PhoneDeleteDialogComponent } from './phone-delete-dialog.component';
import { PhoneUpdateComponent } from './phone-update.component';
import { PersonalInfoService } from '../services/personalInfo.service';

@Component({
  selector: 'jhi-phone',
  templateUrl: './phone.component.html',
  styleUrls: ['./phone.component.scss']
})
export class PhoneComponent implements OnInit, OnDestroy {
  phones?: Phone[];
  eventSubscriber?: Subscription;

  country?: Country;

  @Input() infoId!: number;

  constructor(
    protected phoneService: PhoneService,
    protected eventManager: JhiEventManager,
    protected modalService: NgbModal,
    protected personalInfoService: PersonalInfoService
  ) {}

  loadAll(): void {
    this.phoneService.findByDeletedAndInfo(0, this.infoId).subscribe((res: HttpResponse<Phone[]>) => (this.phones = res.body || []));
  }

  ngOnInit(): void {
    this.loadAll();
    this.registerChangeInPhones();
    this.personalInfoService.getCountry(this.infoId).subscribe((res: HttpResponse<Country>) => (this.country = res.body || {}));
  }

  ngOnDestroy(): void {
    if (this.eventSubscriber) {
      this.eventManager.destroy(this.eventSubscriber);
    }
  }

  trackId(index: number, item: Phone): number {
    // eslint-disable-next-line @typescript-eslint/no-unnecessary-type-assertion
    return item.id!;
  }

  registerChangeInPhones(): void {
    this.eventSubscriber = this.eventManager.subscribe('PhoneModification', () => this.loadAll());
  }

  delete(phone: Phone): void {
    const modalRef = this.modalService.open(PhoneDeleteDialogComponent, { size: 'md', backdrop: 'static' });
    modalRef.componentInstance.phone = phone;
  }
  update(phone?: Phone): void {
    const modalRef = this.modalService.open(PhoneUpdateComponent, { size: 'md', backdrop: 'static' });
    modalRef.componentInstance.phone = phone;
    modalRef.componentInstance.infoId = this.infoId;
    modalRef.componentInstance.codePhone = this.country?.codePhone;
    if (phone) {
      modalRef.componentInstance.create = false;
    } else {
      modalRef.componentInstance.create = true;
    }
  }
}
