import { Resolve, Router, ActivatedRouteSnapshot, Routes } from '@angular/router';
import { Phone, CPhone } from 'app/shared/services/model';
import { PhoneService } from 'app/shared/services/phone.service';
import { Injectable } from '@angular/core';
import { Observable, of, EMPTY } from 'rxjs';
import { flatMap } from 'rxjs/operators';
import { HttpResponse } from '@angular/common/http';
import { PhoneUpdateComponent } from './phone-update.component';
import { Authority } from 'app/shared/constants/authority.constants';
import { UserRouteAccessService } from 'app/core/auth/user-route-access-service';

@Injectable({ providedIn: 'root' })
export class PhoneResolve implements Resolve<Phone> {
  constructor(private service: PhoneService, private router: Router) {}

  resolve(route: ActivatedRouteSnapshot): Observable<Phone> | Observable<never> {
    const id = route.params['id'];
    if (id) {
      return this.service.find(id).pipe(
        flatMap((phone: HttpResponse<Phone>) => {
          if (phone.body) {
            return of(phone.body);
          } else {
            this.router.navigate(['404']);
            return EMPTY;
          }
        })
      );
    }
    return of(new CPhone());
  }
}

export const phoneRoute: Routes = [
  {
    path: 'phone/new',
    component: PhoneUpdateComponent,
    resolve: {
      phone: PhoneResolve
    },
    data: {
      authorities: [Authority.USER],
      pageTitle: 'phones'
    },
    canActivate: [UserRouteAccessService]
  },

  {
    path: 'phone/:id/edit',
    component: PhoneUpdateComponent,
    resolve: {
      phone: PhoneResolve
    },
    data: {
      authorities: [Authority.USER],
      pageTitle: 'phones'
    },
    canActivate: [UserRouteAccessService]
  }
];
