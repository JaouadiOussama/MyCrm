import { Phone, CPhone } from 'app/shared/services/model';
import { Component } from '@angular/core';
import { PhoneService } from 'app/shared/services/phone.service';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { JhiEventManager } from 'ng-jhipster';
import { HttpResponse } from '@angular/common/http';
import { ToastrService } from 'ngx-toastr';

@Component({
  templateUrl: './phone-delete-dialog.component.html'
})
export class PhoneDeleteDialogComponent {
  phone?: Phone;

  constructor(
    protected phoneService: PhoneService,
    public activeModal: NgbActiveModal,
    protected toastr: ToastrService,
    protected eventManager: JhiEventManager
  ) {}

  cancel(): void {
    this.activeModal.dismiss();
  }

  confirmDelete(id: number): void {
    this.phoneService.find(id).subscribe((res: HttpResponse<Phone>) => {
      this.phone = res.body || new CPhone();
      this.phone.deleted = 1;
      this.phoneService.update(this.phone).subscribe(() => {
        this.toastr.success('suppression avec succés');
        this.eventManager.broadcast('PhoneModification');
        this.activeModal.close();
      });
    });
  }
}
