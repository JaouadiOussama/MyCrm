import { Component, OnInit } from '@angular/core';
import { PhoneService } from 'app/shared/services/phone.service';
import { ActivatedRoute } from '@angular/router';
import { FormBuilder, Validators } from '@angular/forms';
import { Phone, CPhone, PersonalInfo, Info } from 'app/shared/services/model';
import { Observable } from 'rxjs';
import { HttpResponse } from '@angular/common/http';
import { JhiEventManager } from 'ng-jhipster';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { ToastrService } from 'ngx-toastr';
import { TranslateService } from '@ngx-translate/core';

@Component({
  selector: 'jhi-phone-update',
  templateUrl: './phone-update.component.html',
  styleUrls: ['./phone-update.component.scss']
})
export class PhoneUpdateComponent implements OnInit {
  isSaving = false;
  phone?: Phone;
  create = false;
  infoId = 0;
  codePhone = 0;

  phoneTypes = [{ name: 'FAX' }, { name: 'Entreprise' }, { name: 'Personel' }];

  editForm = this.fb.group({
    id: [],
    phoneNumber: [null, [Validators.required, Validators.pattern(/^-?([0-9]\d*)?$/)]],

    phoneType: [null]
  });

  constructor(
    protected phoneService: PhoneService,
    protected activatedRoute: ActivatedRoute,
    private fb: FormBuilder,
    protected toastr: ToastrService,
    protected eventManager: JhiEventManager,
    public activeModal: NgbActiveModal,
    public translateService: TranslateService
  ) {}

  cancel(): void {
    this.activeModal.dismiss();
  }
  ngOnInit(): void {
    if (this.phone !== undefined) {
      this.updateForm(this.phone);
    }
  }

  updateForm(phone: Phone): void {
    this.editForm.patchValue({
      id: phone.id,
      phoneNumber: phone.phoneNumber,

      phoneType: phone.phoneType
    });
  }

  previousState(): void {
    window.history.back();
  }

  save(): void {
    this.isSaving = true;
    const phone = this.createFromForm();
    if (this.create === false) {
      this.subscribeToSaveResponse(this.phoneService.update(phone));
    } else {
      this.subscribeToSaveResponse(this.phoneService.create(phone));
    }
  }
  private createFromForm(): Phone {
    return {
      ...new CPhone(),
      id: this.editForm.get(['id'])!.value,
      phoneNumber: this.editForm.get(['phoneNumber'])!.value,

      phoneType: this.editForm.get(['phoneType'])!.value,
      deleted: 0,
      info: this.createInfo()
    };
  }
  private createInfo(): Info {
    return {
      id: this.infoId
    };
  }

  protected subscribeToSaveResponse(result: Observable<HttpResponse<Phone>>): void {
    result.subscribe(
      () => this.onSaveSuccess(),
      () => this.onSaveError()
    );
  }

  protected onSaveSuccess(): void {
    this.isSaving = false;
    const entityNameCreated = this.translateService.instant('telephone.message.create');
    const entityNameUpdate = this.translateService.instant('telephone.message.update');
    if (this.create) {
      this.toastr.success(entityNameCreated);
    } else {
      this.toastr.success(entityNameUpdate);
    }
    this.eventManager.broadcast('PhoneModification');
    this.activeModal.close();
  }

  protected onSaveError(): void {
    this.isSaving = false;
  }
}
