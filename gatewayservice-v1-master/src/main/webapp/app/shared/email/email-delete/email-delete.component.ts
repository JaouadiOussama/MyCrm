import { Component, OnInit } from '@angular/core';
import { Email, CEmail } from 'app/shared/services/model';
import { HttpResponse } from '@angular/common/http';
import { ToastrService } from 'ngx-toastr';
import { JhiEventManager } from 'ng-jhipster';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { EmailService } from 'app/shared/services/email.service';
import { TranslateService } from '@ngx-translate/core';

@Component({
  selector: 'jhi-email-delete',
  templateUrl: './email-delete.component.html',
  styleUrls: ['./email-delete.component.scss']
})
export class EmailDeleteComponent implements OnInit {
  email!: Email;

  constructor(
    protected emailService: EmailService,
    public activeModal: NgbActiveModal,
    protected toastr: ToastrService,
    protected eventManager: JhiEventManager,
    public translateService: TranslateService
  ) {}
  ngOnInit(): void {}
  cancel(): void {
    this.activeModal.dismiss();
  }

  confirmDelete(id: number): void {
    this.emailService.find(id).subscribe((res: HttpResponse<Email>) => {
      const entityNameSupp = this.translateService.instant('email.message.supprimerSuccess');
      this.email = res.body || new CEmail();
      this.email.deleated = 1;
      this.emailService.update(this.email).subscribe(() => {
        this.toastr.success(entityNameSupp);
        this.eventManager.broadcast('EmailModification');
        this.activeModal.close();
      });
    });
  }
}
