import { Resolve, Router, ActivatedRouteSnapshot, Routes } from '@angular/router';
import { Email, CEmail } from 'app/shared/services/model';

import { Injectable } from '@angular/core';
import { Observable, of, EMPTY } from 'rxjs';
import { flatMap } from 'rxjs/operators';
import { HttpResponse } from '@angular/common/http';

import { Authority } from 'app/shared/constants/authority.constants';
import { UserRouteAccessService } from 'app/core/auth/user-route-access-service';
import { EmailUpdateComponent } from './email-update/email-update.component';
import { EmailService } from '../services/email.service';

@Injectable({ providedIn: 'root' })
export class EmailResolve implements Resolve<Email> {
  constructor(private service: EmailService, private router: Router) {}

  resolve(route: ActivatedRouteSnapshot): Observable<Email> | Observable<never> {
    const id = route.params['id'];
    if (id) {
      return this.service.find(id).pipe(
        flatMap((email: HttpResponse<Email>) => {
          if (email.body) {
            return of(email.body);
          } else {
            this.router.navigate(['404']);
            return EMPTY;
          }
        })
      );
    }
    return of(new CEmail());
  }
}

export const emailRoute: Routes = [
  {
    path: 'email/new',
    component: EmailUpdateComponent,
    resolve: {
      email: EmailResolve
    },
    data: {
      authorities: [Authority.USER],
      pageTitle: 'emails'
    },
    canActivate: [UserRouteAccessService]
  },

  {
    path: 'email/:id/edit',
    component: EmailUpdateComponent,
    resolve: {
      email: EmailResolve
    },
    data: {
      authorities: [Authority.USER],
      pageTitle: 'emails'
    },
    canActivate: [UserRouteAccessService]
  }
];
