import { Component, OnInit, OnDestroy } from '@angular/core';
import { Email, CEmail, PersonalInfo, Info } from 'app/shared/services/model';
import { Validators, FormBuilder } from '@angular/forms';
import { EmailService } from 'app/shared/services/email.service';
import { ActivatedRoute } from '@angular/router';
import { ToastrService } from 'ngx-toastr';
import { JhiEventManager, JhiEventWithContent } from 'ng-jhipster';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { Observable, Subscription } from 'rxjs';
import { HttpResponse, HttpErrorResponse } from '@angular/common/http';
import { TranslateService } from '@ngx-translate/core';

@Component({
  selector: 'jhi-email-update',
  templateUrl: './email-update.component.html',
  styleUrls: ['./email-update.component.scss']
})
export class EmailUpdateComponent implements OnInit, OnDestroy {
  isSaving = false;
  email?: Email;
  create = false;
  infoId = 0;
  httpErrorListener!: Subscription;

  type = [{ name: 'Entreprise' }, { name: 'Personel' }];
  editForm = this.fb.group({
    id: [],
    email: [null, [Validators.email]],
    emailType: [null, [Validators.required]]
  });

  constructor(
    protected emailService: EmailService,
    protected activatedRoute: ActivatedRoute,
    private fb: FormBuilder,
    protected toastr: ToastrService,
    protected eventManager: JhiEventManager,
    public activeModal: NgbActiveModal,
    public translateService: TranslateService
  ) {
    this.httpErrorListener = this.eventManager.subscribe(
      'gatewayServiceApp.httpError',
      (response: JhiEventWithContent<HttpErrorResponse>) => {
        const httpErrorResponse = response.content;
        const entityNameUniqueUrl = this.translateService.instant('email.message.unique');

        switch (httpErrorResponse.status) {
          case 400:
            this.toastr.error(entityNameUniqueUrl);

            break;
        }
      }
    );
  }

  cancel(): void {
    this.activeModal.dismiss();
  }
  ngOnInit(): void {
    if (this.email !== undefined) {
      this.updateForm(this.email);
    }
  }

  updateForm(email: Email): void {
    this.editForm.patchValue({
      id: email.id,
      email: email.email,

      emailType: email.emailType
    });
  }

  previousState(): void {
    window.history.back();
  }
  // convenience getter for easy access to form fields
  // eslint-disable-next-line @typescript-eslint/tslint/config
  get f() {
    return this.editForm.controls;
  }

  save(): void {
    this.isSaving = true;
    const email = this.createFromForm();
    // stop here if form is invalid
    if (this.editForm.invalid) {
      return;
    }

    if (this.create === false) {
      this.subscribeToSaveResponse(this.emailService.update(email));
    } else {
      this.subscribeToSaveResponse(this.emailService.create(email));
    }
  }
  private createFromForm(): Email {
    return {
      ...new CEmail(),
      id: this.editForm.get(['id'])!.value,
      email: this.editForm.get(['email'])!.value,
      info: this.createInfo(),
      emailType: this.editForm.get(['emailType'])!.value,
      deleated: 0
    };
  }
  private createInfo(): Info {
    return {
      id: this.infoId
    };
  }

  protected subscribeToSaveResponse(result: Observable<HttpResponse<Email>>): void {
    result.subscribe(
      () => this.onSaveSuccess(),
      () => this.onSaveError()
    );
  }

  protected onSaveSuccess(): void {
    this.isSaving = false;
    const entityNameCreated = this.translateService.instant('email.message.create');
    const entityNameUpdate = this.translateService.instant('email.message.update');

    if (this.create) {
      this.toastr.success(entityNameCreated);
    } else {
      this.toastr.success(entityNameUpdate);
    }
    this.eventManager.broadcast('EmailModification');
    this.activeModal.close();
  }

  protected onSaveError(): void {
    this.isSaving = false;
    if (this.httpErrorListener) {
      this.eventManager.destroy(this.httpErrorListener);
    }
  }

  ngOnDestroy(): void {
    if (this.httpErrorListener) {
      this.eventManager.destroy(this.httpErrorListener);
    }
  }
}
