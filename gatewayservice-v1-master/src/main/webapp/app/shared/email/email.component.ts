import { Component, OnInit, Input, OnDestroy } from '@angular/core';
import { Email } from 'app/shared/services/model';
import { HttpResponse } from '@angular/common/http';
import { JhiEventManager } from 'ng-jhipster';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { EmailService } from 'app/shared/services/email.service';
import { Subscription } from 'rxjs';
import { EmailDeleteComponent } from './email-delete/email-delete.component';
import { EmailUpdateComponent } from './email-update/email-update.component';

@Component({
  selector: 'jhi-email',
  templateUrl: './email.component.html',
  styleUrls: ['./email.component.scss']
})
export class EmailComponent implements OnInit, OnDestroy {
  emails?: Email[];
  eventSubscriber?: Subscription;

  @Input() infoId!: number;

  constructor(protected emailService: EmailService, protected eventManager: JhiEventManager, protected modalService: NgbModal) {}

  loadAll(): void {
    this.emailService.findByDeletedAndInfo(0, this.infoId).subscribe((res: HttpResponse<Email[]>) => (this.emails = res.body || []));
  }

  ngOnInit(): void {
    this.loadAll();
    this.registerChangeInEmails();
  }

  ngOnDestroy(): void {
    if (this.eventSubscriber) {
      this.eventManager.destroy(this.eventSubscriber);
    }
  }

  trackId(index: number, item: Email): number {
    // eslint-disable-next-line @typescript-eslint/no-unnecessary-type-assertion
    return item.id!;
  }

  registerChangeInEmails(): void {
    this.eventSubscriber = this.eventManager.subscribe('EmailModification', () => this.loadAll());
  }

  delete(email: Email): void {
    const modalRef = this.modalService.open(EmailDeleteComponent, { size: 'md', backdrop: 'static' });
    modalRef.componentInstance.email = email;
  }
  update(email?: Email): void {
    const modalRef = this.modalService.open(EmailUpdateComponent, { size: 'md', backdrop: 'static' });
    if (email) {
      modalRef.componentInstance.email = email;
      modalRef.componentInstance.infoId = this.infoId;
      modalRef.componentInstance.create = false;
    } else {
      modalRef.componentInstance.email = email;
      modalRef.componentInstance.infoId = this.infoId;

      modalRef.componentInstance.create = true;
    }
  }
}
