import { NgModule } from '@angular/core';

import { GatewayServiceSharedModule } from 'app/shared/shared.module';
import { RouterModule } from '@angular/router';
import { EmployeeListComponent } from './employee-list/employee-list.component';
import { MyEmployeeState } from './my-employee.route';

@NgModule({
  imports: [GatewayServiceSharedModule, RouterModule.forChild(MyEmployeeState)],
  declarations: [EmployeeListComponent]
})
export class MyEmployeeModule {}
