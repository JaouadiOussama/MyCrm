import { Routes } from '@angular/router';
import { employeeListRoute } from 'app/my-employee/employee-list/employee-list.route';

const MY_EMPLOYEE_ROUTES = [employeeListRoute];

export const MyEmployeeState: Routes = [
  {
    path: '',
    children: MY_EMPLOYEE_ROUTES
  }
];
