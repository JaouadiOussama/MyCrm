import { Route } from '@angular/router';
import { EmployeeListComponent } from 'app/my-employee/employee-list/employee-list.component';

export const employeeListRoute: Route = {
  path: 'employeelist',
  component: EmployeeListComponent
  /* data: {
    authorities: [],
    pageTitle: 'accountlist.title'
  }*/
};
