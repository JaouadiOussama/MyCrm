import { Component, OnInit, OnDestroy } from '@angular/core';
import { Employee } from 'app/shared/services/model';
import { Subscription } from 'rxjs';
import { JhiEventManager } from 'ng-jhipster';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { HttpResponse, HttpHeaders } from '@angular/common/http';
import { EmployeeService } from 'app/shared/services/employee.service';

@Component({
  selector: 'jhi-employee-list',
  templateUrl: './employee-list.component.html',
  styleUrls: ['./employee-list.component.scss']
})
export class EmployeeListComponent implements OnInit, OnDestroy {
  employees: Employee[] = [];
  enventSubscriber?: Subscription;
  checkedClicked = false;
  allButtonChecked = false;
  sort = false;

  /* pagination */
  page = 1;
  pageSize = 2;
  totalItems = 0;

  constructor(protected employeeService: EmployeeService, protected eventManager: JhiEventManager, protected modalService: NgbModal) {}

  loadAll(): void {
    this.employeeService.query().subscribe((res: HttpResponse<Employee[]>) => this.onSuccess(res.body, res.headers));
  }
  private onSuccess(employees: Employee[] | null, headers: HttpHeaders): void {
    this.totalItems = Number(headers.get('X-Total-Count'));
    this.employees = employees || [];
    this.employees.sort((a, b) => {
      return a.persLastName.localeCompare(b.persLastName);
    });
  }

  ngOnInit(): void {
    this.loadAll();
    this.registerChangeContact();
  }
  trier(): void {
    this.employees = this.employees.reverse();
  }
  ngOnDestroy(): void {
    if (this.enventSubscriber) {
      this.eventManager.destroy(this.enventSubscriber);
    }
  }
  reload(): void {
    this.loadAll();
    this.checkedClicked = false;
    this.allButtonChecked = false;
  }

  registerChangeContact(): void {
    this.enventSubscriber = this.eventManager.subscribe('modifEmployee', () => this.reload());
  }

  trackId(index: number, item: Employee): number {
    // eslint-disable-next-line @typescript-eslint/no-unnecessary-type-assertion
    return item.id!;
  }

  checkAll(event: any): void {
    if (event.target.checked) {
      this.checkedClicked = true;
      this.allButtonChecked = true;
    } else {
      this.checkedClicked = false;
      this.allButtonChecked = false;
    }
  }

  checkOne(): void {
    this.checkedClicked = !this.checkedClicked;
    /* if (!this.allButtonChecked) {
      this.checkedClicked = false;
    } else {
      this.checkedClicked = true;
    }*/
  }
}
