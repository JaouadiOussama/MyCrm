/**
 * View Models used by Spring MVC REST controllers.
 */
package com.theteamconsulting.tn.web.rest.vm;
