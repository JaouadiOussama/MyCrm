import { ContactListComponent } from 'app/my-contact/contact-list/contact-list.component';
import { ComponentFixture, TestBed } from '@angular/core/testing';
import { ContactService } from 'app/shared/services/my-contact.service';
import { GatewayServiceTestModule } from '../../test.module';
import { HttpHeaders, HttpResponse } from '@angular/common/http';
import { of } from 'rxjs';
import { CContact } from 'app/shared/services/model';

describe('Component Tests', () => {
  describe('Contact list Component', () => {
    let comp: ContactListComponent;
    let fixture: ComponentFixture<ContactListComponent>;
    let service: ContactService;

    beforeEach(() => {
      TestBed.configureTestingModule({
        imports: [GatewayServiceTestModule],
        declarations: [ContactListComponent]
      })
        .overrideTemplate(ContactListComponent, '')
        .compileComponents();

      fixture = TestBed.createComponent(ContactListComponent);
      comp = fixture.componentInstance;
      service = fixture.debugElement.injector.get(ContactService);
    });

    it('Should call load all on init', () => {
      // GIVEN
      const headers = new HttpHeaders().append('link', 'link;link');
      const entity = new CContact();
      entity.id = 123;
      spyOn(service, 'findByDeleted').and.returnValue(
        of(
          new HttpResponse({
            body: [entity],
            headers
          })
        )
      );

      // WHEN
      comp.ngOnInit();

      // THEN
      expect(service.findByDeleted).toHaveBeenCalled();
      expect(comp.contacts && comp.contacts[0]).toEqual(jasmine.objectContaining({ id: 123 }));
    });
  });
});
